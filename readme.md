# Astra
## Code testing tool (using JUnit 5)

Desired functionalities:

1. Users and admin can login
2. Admins can manage users
3. Users can upload .jar files of their solutions
4. Admins can upload .java files of their tests
5. The system can run maven tests based on user .jar and
admin .java combination
6. Users can see results of the testing

## Building static resources

1. Install node and update npm (Linux)
  - `curl -sL https://deb.nodesource.com/setup_7.x | sudo -E bash -`
  -  `sudo apt-get install -y nodejs`
  -  `npm install npm@latest -g`
  
2. Install gulp globally
  - `npm install -g gulp`
3. Navigate to the root of the project
4. Install node modules
  - `npm install`
  - wait
5. Build the resources
  - `gulp`