package hr.fer.oop.forms;


import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class UserBulkForm {

    @NotNull
    private String userList;

    @NotNull
    private Long userGroupId;

    public String getUserList() {
        return userList;
    }

    public void setUserList(String userList) {
        this.userList = userList;
    }

    public Long getUserGroupId() {
        return userGroupId;
    }

    public void setUserGroupId(Long userGroupId) {
        this.userGroupId = userGroupId;
    }
}
