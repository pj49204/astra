package hr.fer.oop.forms;


import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class AssignmentForm {

    @NotNull
    @Size(min=3, max=255)
    private String name;

    @NotNull
    @Size(min=3, max=255)
    private String description;

    @NotNull
    private Long exerciseId;

    private String testsPath;

    private String testsSourcePath;

    public String getTestsSourcePath() {
        return testsSourcePath;
    }

    public void setTestsSourcePath(String testsSourcePath) {
        this.testsSourcePath = testsSourcePath;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getExerciseId() {
        return exerciseId;
    }

    public void setExerciseId(Long exerciseId) {
        this.exerciseId = exerciseId;
    }

    public void setTestsPath(String testsPath) {
        this.testsPath = testsPath;
    }

    public String getTestsPath() {
        return testsPath;
    }
}
