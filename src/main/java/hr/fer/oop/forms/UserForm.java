package hr.fer.oop.forms;


import hr.fer.oop.entities.User;
import hr.fer.oop.entities.UserGroup;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.beans.BeanUtils;

import javax.persistence.Column;
import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Set;
import java.util.stream.Collectors;

public class UserForm {

    @NotNull
    @Size(max=255,min = 1)
    private String username;

    private String password;

    @NotNull
    @NotBlank
    private String role;

    @NotNull
    @Size(max=255,min=1)
    private String firstName;

    @NotNull
    @Size(max=255,min=1)
    private String lastName;

    @NotNull
    @Size(max=12,min=10)
    private String JMBAG;

    @NotNull
    private Set<Long> userGroupIds;

    public UserForm () {}

    public UserForm (User user) {
        BeanUtils.copyProperties(user, this);
        this.userGroupIds = user.getUserGroups()
                .stream()
                .map(UserGroup::getId)
                .collect(Collectors.toSet());
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getJMBAG() {
        return JMBAG;
    }

    public void setJMBAG(String JMBAG) {
        this.JMBAG = JMBAG;
    }

    public Set<Long> getUserGroupIds() {
        return userGroupIds;
    }

    public void setUserGroupIds(Set<Long> userGroupIds) {
        this.userGroupIds = userGroupIds;
    }
}
