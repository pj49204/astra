package hr.fer.oop.forms;


import hr.fer.oop.entities.Exercise;
import hr.fer.oop.entities.UserGroup;
import org.springframework.beans.BeanUtils;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class ExerciseForm {

    @NotNull
    @Size(min=3, max=255)
    private String name;

    @NotNull
    @DateTimeFormat(pattern="u-M-d")
    private LocalDate beginTime;

    @NotNull
    @DateTimeFormat(pattern="u-M-d")
    private LocalDate endTime;

    @NotNull
    private Set<Long> userGroupIds;

    public ExerciseForm () {}

    public ExerciseForm (Exercise exercise) {
        BeanUtils.copyProperties(exercise, this);
        this.userGroupIds = exercise.getUserGroups()
                .stream()
                .map(UserGroup::getId)
                .collect(Collectors.toSet());
    }

    public String getName() {
        return name;
    }

    public LocalDate getBeginTime() {
        return beginTime;
    }

    public LocalDate getEndTime() {
        return endTime;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setBeginTime(LocalDate beginTime) {
        this.beginTime = beginTime;
    }

    public void setEndTime(LocalDate endTime) {
        this.endTime = endTime;
    }

    public void setBeginTime(String beginTime) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-mm-dd");
        this.beginTime = LocalDate.parse(beginTime, formatter);;
    }

    public void setEndTime(String endTime) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-mm-dd");
        this.endTime = LocalDate.parse(endTime, formatter);;
    }

    public Set<Long> getUserGroupIds() {
        return userGroupIds;
    }

    public void setUserGroupIds(Set<Long> userGroupIds) {
        this.userGroupIds = userGroupIds;
    }
}
