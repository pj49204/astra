package hr.fer.oop.testRunner;

import org.junit.platform.commons.util.ReflectionUtils;
import org.junit.platform.launcher.Launcher;
import org.junit.platform.launcher.LauncherDiscoveryRequest;
import org.junit.platform.launcher.core.LauncherDiscoveryRequestBuilder;
import org.junit.platform.launcher.core.LauncherFactory;
import org.springframework.stereotype.Component;

import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Stream;

import static org.junit.platform.engine.discovery.ClassNameFilter.includeClassNamePatterns;
import static org.junit.platform.engine.discovery.DiscoverySelectors.selectPackage;


/**
 * Runs JUnit 4 and 5 tests using JUnit platform launcher ( {@link Launcher} )
 * User gives string of tests and tested class' path ( executable .jar or .class files )
 * Logs on Level.FINE
 * Created by pavao on 24.11.16..
 */
@Component
public class TestRunner {

    private String classPath;
    private String testPath;

    private final Logger LOG = Logger.getLogger(TestRunner.class.getName());

    /**
     * @return tested class' path
     */
    public String getClassPath() {
        return classPath;
    }

    /**
     * @return test path
     */
    public String getTestPath() {
        return testPath;
    }

    /**
     * @param classPath sets classPath
     */
    public void setClassPath(String classPath) {
        this.classPath = classPath;
    }

    /**
     * @param testPath sets testPath
     */
    public void setTestPath(String testPath) {
        this.testPath = testPath;
    }


    /**
     * Empty constructor
     */
    public TestRunner() {
    }

    /**
     * Constructor that takes class and test paths and sets its' values
     *
     * @param classPath tested class' path
     * @param testPath  test's path
     */
    public TestRunner(String classPath, String testPath) {
        this.classPath = classPath;
        this.testPath = testPath;
    }

    private CustomTestExecutionSummary runAll() { //todo: TestExecutionSummary
        URL[] urls = Stream.of(
                Paths.get(classPath),
                Paths.get(testPath)
        )
                .map(path -> {
                    try {
                        return path.toUri().toURL();
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                        return null;
                    }
                })
                .toArray(URL[]::new);
        System.out.println(Arrays.toString(urls));//novo
        ClassLoader parentClassLoader = ReflectionUtils.getDefaultClassLoader();
        ClassLoader customClassLoader = URLClassLoader.newInstance(urls, parentClassLoader);
        Thread.currentThread().setContextClassLoader(customClassLoader);
        LauncherDiscoveryRequest request = LauncherDiscoveryRequestBuilder.request()
                .selectors(selectPackage("hr.fer.oop"))
                .filters(includeClassNamePatterns(".*Test"))
                .build();
        Launcher launcher = LauncherFactory.create();
        CustomTestListener listener = new CustomTestListener();
//        launcher.registerTestExecutionListeners(listener);
        launcher.execute(request, listener);
        CustomTestExecutionSummary summary = listener.getSummary();
        summary.printTo(new PrintWriter(System.out));
        summary.printFailuresTo(new PrintWriter(System.out));
        return summary;
    }

    /**
     * Executes tests and returns test execution summary
     *
     * @return summary of executed tests
     */
    public CustomTestExecutionSummary start() {
        setupLogger();
        System.out.println(TestRunner.class.getName());

        LOG.fine(System.getProperty("user.dir"));

        return runAll();
    }

    private void setupLogger() {
        Level logLevel = Level.FINE;
        Logger root = LOG;

        while (root.getParent() != null)
            root = root.getParent();

        root.setLevel(logLevel);
        for (Handler handler : root.getHandlers()) {
            handler.setLevel(logLevel);
        }
    }
}
