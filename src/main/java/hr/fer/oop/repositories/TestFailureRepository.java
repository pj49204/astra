package hr.fer.oop.repositories;

import hr.fer.oop.entities.TestFailure;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by pavao on 21.12.16..
 */
public interface TestFailureRepository extends CrudRepository<TestFailure, Long> {
}
