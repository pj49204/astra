package hr.fer.oop.repositories;

import hr.fer.oop.entities.Assignment;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AssignmentRepository extends CrudRepository<Assignment, Long>{

}
