package hr.fer.oop.repositories;

import hr.fer.oop.entities.UserGroup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by pavao on 10.01.17..
 */
@Repository
public interface UserGroupRepository extends JpaRepository<UserGroup, Long> {
    UserGroup findByName(String name);
    @Query(value = "select g from UserGroup g join g.exercises e where current_timestamp between e.beginTime and e.endTime and g.id = ?1")
    UserGroup findOneByCurrentExercises(Long id);
}
