package hr.fer.oop.repositories;

import hr.fer.oop.entities.UploadSourceEntry;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UploadSourceEntryRepository extends JpaRepository<UploadSourceEntry, Long> {

}
