package hr.fer.oop.repositories;

import hr.fer.oop.entities.Exercise;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.security.Timestamp;
import java.time.LocalDateTime;
import java.util.Iterator;

/**
 * Created by pavao on 10.01.17..
 */
@Repository
public interface ExerciseRepository extends JpaRepository<Exercise, Long> {
//    Iterable<Exercise> findByBeginTimeBeforeAndEndTimeAfter(LocalDateTime time);
    Exercise findByName(String name);
    @Query(value = "select e from Exercise e where current_timestamp between  e.beginTime and e.endTime order by e.beginTime desc")
    Iterable<Exercise> findAllCurrent();

    @Query(value = "select e from Exercise e join e.userGroups g where current_timestamp between  e.beginTime and e.endTime and g.id = ?1 order by e.beginTime desc ")
    Iterable<Exercise> findAllCurrentByUserGroupId(Long id);



}
