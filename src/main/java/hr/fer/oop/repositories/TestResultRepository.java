package hr.fer.oop.repositories;

import hr.fer.oop.entities.TestResult;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TestResultRepository extends CrudRepository<TestResult, Long>{
    public TestResult findByUploadId(Long id);
}
