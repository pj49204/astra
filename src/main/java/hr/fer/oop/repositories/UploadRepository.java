package hr.fer.oop.repositories;

import hr.fer.oop.entities.Upload;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UploadRepository extends CrudRepository<Upload, Long>{

}
