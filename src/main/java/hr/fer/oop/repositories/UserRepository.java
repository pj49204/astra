package hr.fer.oop.repositories;

import hr.fer.oop.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    // @Query("select user from User user where user.username= :username")
    User findByUsername(String username);
    // @Query("select user from User user where user.role= :userRole")
    Iterable<User> findAllByRole(String userRole);
}
