package hr.fer.oop.profiles;

import hr.fer.oop.entities.Assignment;
import hr.fer.oop.entities.Exercise;
import hr.fer.oop.entities.User;
import hr.fer.oop.entities.UserGroup;
import hr.fer.oop.utils.parser.Parser;
import hr.fer.oop.repositories.AssignmentRepository;
import hr.fer.oop.repositories.ExerciseRepository;
import hr.fer.oop.repositories.UserGroupRepository;
import hr.fer.oop.repositories.UserRepository;
import hr.fer.oop.services.AssignmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.validation.ConstraintViolationException;
import java.util.*;

/**
 * Created by pavao on 22.12.16..
 */
@Configuration
@Profile(value = "dev")
public class DevConfigurations implements CommandLineRunner {
    private UserRepository userRepository;
    @Autowired
    public void setUserRepository(UserRepository userRepository){
        this.userRepository=userRepository;
    }

    private AssignmentRepository assignmentRepository;
    @Autowired
    public void setAssignmentRepository(AssignmentRepository assignmentRepository){
        this.assignmentRepository=assignmentRepository;
    }



    private ExerciseRepository exerciseRepository;

    private AssignmentService assignmentService;


    private UserGroupRepository userGroupRepository;

    @Autowired
    public void setuserGroupRepository(UserGroupRepository userGroupRepository){
        this.userGroupRepository = userGroupRepository;
    }

    @Autowired
    public void setExerciseRepository(ExerciseRepository exerciseRepository){
        this.exerciseRepository = exerciseRepository;
    }

    private PasswordEncoder passwordEncoder;
    @Autowired
    public void setPasswordEncoder(PasswordEncoder passwordEncoder){
        this.passwordEncoder= new BCryptPasswordEncoder();
    }
    /*
    * Initialization of 2 users
    * User user with password password and role USER
    * User admin with password admin and role ADMIN
    * Initialization of 1 assignment
    * Assignment assignment with name "Ime labosa" and description "Opis labosa" , id = 1
    * */
    @Override
    public void run(String... strings) throws Exception {
        try{
            User user = new User(
                    "user",
                    passwordEncoder.encode("password"),
                    "USER"
            );

            userRepository.save(user);

        } catch (ConstraintViolationException e){
            System.out.println(e.getMessage());
        }

        try{

            User admin = new User(
                    "admin",
                    passwordEncoder.encode("admin"),
                    "ADMIN",
                    "Admin",
                    "Adminic",
                    "0000000000"
            );

            userRepository.save(admin);

        } catch (ConstraintViolationException e){
            System.out.println(e.getMessage());
        }



        Exercise exercise = new Exercise();

        try{
            System.out.println(exercise);
            Assignment assignment = new Assignment("Ime zadatka", "Opis zadatka", exercise);
            exercise.getAssignments().add(assignment);
            exercise = exerciseRepository.save(exercise);
            assignmentRepository.save(assignment);
            System.out.println(exercise);
        }catch (Exception e){
            System.out.println(e.getMessage());
            e.printStackTrace();
        }

        try{
            UserGroup userGroup = userGroupRepository.save(new UserGroup("OOP2017"));

            User user1 = userRepository.findByUsername("user");
            User user2 = userRepository.findByUsername("admin");
//            user1.getUserGroups().add(userGroup);
//            user2.getUserGroups().add(userGroup);
            userGroup.setUsers(new HashSet<User>(){{
                add(userRepository.findByUsername("user"));
                add(userRepository.findByUsername("admin"));
            }});

//            exercise.getUserGroups().add(userGroup);
            Exercise finalExercise1 = exercise;
            userGroup.setExercises(new HashSet<Exercise>(){{
                add(finalExercise1);
            }});
//            exerciseRepository.save(exercise);
//            userRepository.save(user1);
//            userRepository.save(user2);
            userGroupRepository.save(userGroup);
        }catch (Exception e){
            System.out.println(e.getMessage());
            e.printStackTrace();
        }

//        try{
//            userGroupService.removeUserFromUserGroup("admin",userGroupService.getUserGroup("OOP2017"));
//        }catch (Exception e){
//            System.out.println(e.getMessage());
//        }

//        try{
//            UserGroup userGroups = userGroupRepository.findOneByCurrentExercises(userGroupService.getUserGroup("OOP2017").getId());
//            System.out.println(userGroups);
//            Hibernate.initialize(userGroups.getExercises());
//            userGroups.getExercises().forEach(System.out::println);
//        }catch (Exception e){
//            e.printStackTrace();
//        }

        try{
            System.out.println("testDivision4");
            System.out.println(Parser.parseMethodBody("src/main/resources/CalculatorTests/hr/fer/oop/CalculatorDivisionTest.java", "testDivision4"));
        }catch (Exception e){
            e.printStackTrace();
        }


    }
}
