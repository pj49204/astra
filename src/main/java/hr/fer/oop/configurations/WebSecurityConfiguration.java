package hr.fer.oop.configurations;

import hr.fer.oop.handlers.CustomAccessDeniedHandler;
import hr.fer.oop.repositories.UserRepository;
import hr.fer.oop.services.LoginService;
import hr.fer.oop.services.auth.CurrentUserServiceImplementation;
import hr.fer.oop.services.jpa.UserGroupServiceImplementation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {

    private CustomAccessDeniedHandler customAccessDeniedHandler;

    public WebSecurityConfiguration(CustomAccessDeniedHandler customAccessDeniedHandler) {
        this.customAccessDeniedHandler = customAccessDeniedHandler;
    }


    @Override
    protected void configure(HttpSecurity http) throws Exception {
        /*
        * za pristup h2-console
        * todo: pristup bez ovih linija
        * */
        http.csrf().disable();
        http.headers().frameOptions().disable();

        http
                .formLogin()
                    .loginPage("/login")
                    .defaultSuccessUrl("/", true)
                    .and()
                .logout()
                    .permitAll()
                    .and()
                .authorizeRequests()
                    .antMatchers("/","/home").permitAll()
                    .antMatchers("/registration","/login").anonymous()
                    .antMatchers("/edit_*/**", "/delete_*/**", "/create_*/**").hasAuthority("ADMIN")
                    .antMatchers("/assignments", "/exercies", "/users", "/user_groups").hasAuthority("ADMIN")
                    .anyRequest().authenticated();

        http.exceptionHandling().accessDeniedHandler(customAccessDeniedHandler);

    }

    private UserRepository userRepository;
    @Autowired
    public void setUserRepository(UserRepository userRepository){
        this.userRepository = userRepository;
    }

    @SuppressWarnings("SpringJavaAutowiringInspection")
    @Autowired
    private LoginService userDetailsService;


    @Bean
    public DaoAuthenticationProvider authenticationProvider(){
        DaoAuthenticationProvider authenticationProvider = new DaoAuthenticationProvider();
        authenticationProvider.setUserDetailsService(userDetailsService);
        authenticationProvider.setPasswordEncoder(passwordEncoder());
        return authenticationProvider;
    }

    @Bean
    public PasswordEncoder passwordEncoder(){
        PasswordEncoder encoder = new BCryptPasswordEncoder();
        return encoder;
    }



    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception{
        authenticationManagerBuilder.userDetailsService(userDetailsService);
        authenticationManagerBuilder.authenticationProvider(authenticationProvider());

    }


}
