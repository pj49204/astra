package hr.fer.oop.configurations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.annotation.AsyncConfigurerSupport;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.Executor;

/**
 * Configuration for asyncExecutor
 *
 * @author Pavao Jerebić
 */
@Configuration
@EnableAsync
public class AsyncTaskExecutorConfiguration extends AsyncConfigurerSupport {

    /**
     * Bean {@link ThreadPoolTaskExecutor}<br/>
     * Core pool size = 7<br/>
     * Maximum pool size = 7<br/>
     * Queue capacity = 100
     *
     * @return Task Executor
     */
    @Bean(name = "threadPoolTaskExecutor")
    public TaskExecutor threadPoolTaskExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(7);
        executor.setMaxPoolSize(7);
        executor.setQueueCapacity(1000);
        executor.initialize();
        return executor;
    }

    private TaskExecutor taskExecutor;

    @Autowired
    public void setTaskExecutor(TaskExecutor taskExecutor) {
        this.taskExecutor = taskExecutor;
    }

    /**
     * Returns {@link ThreadPoolTaskExecutor} for Async tasks executor
     * Core pool size = 7
     * Maximum pool size = 7
     * Queue capacity = 100
     *
     * @return {@link ThreadPoolTaskExecutor}
     */
    @Override
    public Executor getAsyncExecutor() {
        return taskExecutor;
    }

}
