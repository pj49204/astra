package hr.fer.oop.controllers;

import hr.fer.oop.entities.User;
import hr.fer.oop.entities.UserGroup;
import hr.fer.oop.forms.UserBulkForm;
import hr.fer.oop.forms.UserForm;
import hr.fer.oop.services.UserGroupService;
import hr.fer.oop.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;
import java.util.*;

@Controller
public class UsersController {

    private UserService userService;

    @Autowired
    public void setUserService (UserService userService) {
        this.userService = userService;
    }

    private UserGroupService userGroupService;

    @Autowired
    public void setUserGroupService(UserGroupService userGroupService) {
        this.userGroupService = userGroupService;
    }

    @RequestMapping("/users")
    public String index(Model model){
        model.addAttribute("users", userService.allUsers());
        return "users/index";
    }

    @PreAuthorize("@currentUserServiceImplementation.canSeeUser(principal, #id)")
    @RequestMapping("/user/{id}")
    public String show(@PathVariable Long id, Model model){
        model.addAttribute("user", userService.getUser(id));
        return "users/show";
    }

    @RequestMapping("/create_user")
    public String create(Model model){
        model.addAttribute("userForm", new UserForm());
        model.addAttribute("userGroups", userGroupService.allUserGroups());
        model.addAttribute("roles", User.availableRoles);
        return "users/create";
    }

    @RequestMapping(value = "/create_user",  method = RequestMethod.POST)
    public String store(@Valid UserForm userForm, BindingResult bindingResult, Model model){
        if (bindingResult.hasErrors()) {
            model.addAttribute("userGroups", userGroupService.allUserGroups());
            model.addAttribute("roles", User.availableRoles);
            return "users/create";
        }
        User user = userService.saveUser(userForm);

        return "redirect:/user/" + user.getId();
    }

    @RequestMapping("/create_user_bulk")
    public String create_bulk(Model model){
        model.addAttribute("userBulkForm", new UserBulkForm());
        model.addAttribute("userGroups", userGroupService.allUserGroups());
        return "users/create_bulk";
    }

    @RequestMapping(value = "/create_user_bulk",  method = RequestMethod.POST)
    public String store_bulk(@Valid UserBulkForm userBulkForm, BindingResult bindingResult, Model model){
        if (bindingResult.hasErrors()) {
            model.addAttribute("userGroups", userGroupService.allUserGroups());
            return "users/create_bulk";
        }
        // Save users

        Set<User> users = new HashSet<>();
        String[] data = userBulkForm.getUserList().split("\\r?\\n");
        UserGroup userGroup = userGroupService.getUserGroup(userBulkForm.getUserGroupId());
        for(String s : data){ // data format : username,password,firstName,lastName,JMBAG
            try{
                String[] s1 = s.split(",");
                String username, password, firstName, lastName, JMBAG;
                username = s1[0];
                password = s1[1];
                firstName = s1[2];
                lastName = s1[3];
                JMBAG = s1[4];
                User user = new User(username, password, "USER", firstName, lastName, JMBAG);
                user = new User(user, userGroup);
                users.add(user);
            }catch(Exception e){
                e.printStackTrace();
            }
        }
        userGroup.getUsers().addAll(users);
        userService.saveUserList(new ArrayList<User>(){{addAll(users);}});
        return "redirect:/users";
    }

    @RequestMapping("/edit_user/{id}")
    public String edit(@PathVariable Long id, Model model) {
        UserForm userForm = new UserForm(userService.getUser(id));
        model.addAttribute("id", id);
        model.addAttribute("userForm", userForm);
        model.addAttribute("userGroups", userGroupService.allUserGroups());
        model.addAttribute("roles", User.availableRoles);
        return "users/edit";
    }

    @RequestMapping(value = "/edit_user/{id}", method = RequestMethod.POST)
    public String update(@PathVariable Long id, @Valid UserForm userForm, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            System.out.println(bindingResult.getAllErrors());
            model.addAttribute("id", id);
            model.addAttribute("userGroups", userGroupService.allUserGroups());
            model.addAttribute("roles", User.availableRoles);
            return "users/edit";
        }
        userService.updateUser(id, userForm);
        return "redirect:/user/" + id;
    }

    @RequestMapping(value = "/delete_user/{id}", method = RequestMethod.POST)
    public String delete(@PathVariable Long id) {
        userService.deleteUser(id);
        return "redirect:/users";
    }


}
