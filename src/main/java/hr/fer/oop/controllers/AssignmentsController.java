package hr.fer.oop.controllers;

import hr.fer.oop.entities.Assignment;
import hr.fer.oop.entities.User;
import hr.fer.oop.forms.AssignmentForm;
import hr.fer.oop.security.CustomUserDetails;
import hr.fer.oop.services.AssignmentService;
import hr.fer.oop.services.jpa.ExerciseServiceImplementation;
import hr.fer.oop.services.jpa.UserServiceImplementation;
import hr.fer.oop.utils.unzip.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

@Controller
public class AssignmentsController {

    private AssignmentService assignmentService;

    @Autowired
    public void setAssignmentService (AssignmentService assignmentService) {
        this.assignmentService = assignmentService;
    }

    private Environment env;

    @Autowired
    public void setEnv (Environment env) {
        this.env = env;
    }

    private ExerciseServiceImplementation exerciseServiceImplementation;

    @Autowired
    public void setExerciseServiceImplementation (ExerciseServiceImplementation exerciseServiceImplementation) {
        this.exerciseServiceImplementation = exerciseServiceImplementation;
    }

    private UserServiceImplementation userServiceImplementation;

    @Autowired
    public void setUserServiceImplementation (UserServiceImplementation userServiceImplementation) {
        this.userServiceImplementation = userServiceImplementation;
    }

    @RequestMapping("/assignments")
    public String index(Model model){
        model.addAttribute("assignments", assignmentService.allAssignments());
        return "assignments/index";
    }

    @RequestMapping("/assignment/{id}")
    @PreAuthorize("@currentUserServiceImplementation.canSeeAssignment(principal,#id)")
    public String show(@PathVariable Long id, Model model){
        Assignment assignment =assignmentService.getAssignmentById(id);
        model.addAttribute("assignment", assignment);
        CustomUserDetails userDetails = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (userDetails.getUser().getRole().equals("ADMIN")) {
            model.addAttribute("uploads", assignment.getUploads());
        } else {
            User user = userServiceImplementation.getUser(userDetails.getUser().getId());
            model.addAttribute("uploads", user.getUploads());
        }
        return "assignments/show";
    }

    @RequestMapping("/create_assignment")
    public String create(@RequestParam(value = "exerciseId", required = false) Long exerciseId, Model model) {
        AssignmentForm assignmentForm = new AssignmentForm();
        if (exerciseId != null) {
            assignmentForm.setExerciseId(exerciseId);
        }
        model.addAttribute("assignmentForm", assignmentForm);
        model.addAttribute("exercises", exerciseServiceImplementation.allExercises());
        return "assignments/create";
    }

    @RequestMapping(value = "/create_assignment",  method = RequestMethod.POST)
    public String store(@Valid AssignmentForm assignmentForm, BindingResult bindingResult,
                        @RequestParam("testsFile") MultipartFile testsFile,
                        @RequestParam("testsSourceFile") MultipartFile testsSourceFile, Model model){
        if (bindingResult.hasErrors()) {
            model.addAttribute("exercises", exerciseServiceImplementation.allExercises());
            return "assignments/create";
        }
        try {
            Map<String, String> paths = handleTestsFiles(testsFile, testsSourceFile);
            assignmentForm.setTestsPath(paths.get("testsFilePath"));
            assignmentForm.setTestsSourcePath(paths.get("testsSourceFilePath"));
        } catch(Exception e) {
            model.addAttribute("exercises", exerciseServiceImplementation.allExercises());
            model.addAttribute("error", e.getMessage());
            return "assignments/create";
        }
        Assignment assignment = assignmentService.saveAssignment(assignmentForm);
        return "redirect:/assignment/" + assignment.getId();
    }

    @RequestMapping("/edit_assignment/{id}")
    public String edit(@PathVariable Long id, Model model) {
        model.addAttribute("id", id);
        model.addAttribute("assignmentForm", assignmentService.getAssignmentById(id));
        model.addAttribute("exercises", exerciseServiceImplementation.allExercises());
        return "assignments/edit";
    }

    @RequestMapping(value = "/edit_assignment/{id}", method = RequestMethod.POST)
    public String update(@PathVariable Long id, @Valid AssignmentForm assignmentForm, BindingResult bindingResult,
                         @RequestParam("testsFile") MultipartFile testsFile,
                         @RequestParam("testsSourceFile") MultipartFile testsSourceFile, Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("id", id);
            model.addAttribute("exercises", exerciseServiceImplementation.allExercises());
            return "assignments/edit";
        }
        try {
            Map<String, String> paths = handleTestsFiles(testsFile, testsSourceFile);
            assignmentForm.setTestsPath(paths.get("testsFilePath"));
            assignmentForm.setTestsSourcePath(paths.get("testsSourceFilePath"));
            assignmentService.updateAssignment(id, assignmentForm);
        } catch(Exception e) {
            // TODO: redirect back with error
            model.addAttribute("id", id);
            model.addAttribute("exercises", exerciseServiceImplementation.allExercises());
            model.addAttribute("error", e.getMessage());
            return "assignments/edit";
        }
        return "redirect:/assignment/" + id;
    }

    @RequestMapping(value = "/delete_assignment/{id}", method = RequestMethod.POST)
    public String delete(@PathVariable Long id) {
        assignmentService.deleteAssignment(id);
        return "redirect:/assignments";
    }

    private Map<String, String> handleTestsFiles (MultipartFile testsFile, MultipartFile testsSourceFile) throws Exception {
        HashMap<String, String> result = new HashMap<>();
        String directory = String.format("%s/%s", env.getProperty("astra.uploadDirectory"), "assignment-tests");
        FileUtils runnableFileUtils = new FileUtils(directory, "application/x-java-archive");
        FileUtils sourceFileUtils = new FileUtils(directory, "application/zip", "application/octet-stream");
        result.put("testsFilePath", runnableFileUtils.uploadFile(testsFile, "tests"));
        result.put("testsSourceFilePath", sourceFileUtils.unzipFile(testsSourceFile, "tests-source").getDirectoryPath());
        return result;
    }

}
