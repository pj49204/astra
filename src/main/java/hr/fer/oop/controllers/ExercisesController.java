package hr.fer.oop.controllers;

import hr.fer.oop.entities.Exercise;
import hr.fer.oop.entities.UserGroup;
import hr.fer.oop.forms.ExerciseForm;
import hr.fer.oop.services.ExerciseService;
import hr.fer.oop.services.UserGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;

@Controller
public class ExercisesController {

    private ExerciseService exerciseService;

    @Autowired
    public void setExerciseService (ExerciseService exerciseService) {
        this.exerciseService = exerciseService;
    }

    private UserGroupService userGroupService;

    @Autowired
    public void setUserGroupService(UserGroupService userGroupService){
        this.userGroupService = userGroupService;
    }

    @RequestMapping("/exercises")
    public String index(Model model){
        model.addAttribute("exercises", exerciseService.allExercises());
        return "exercises/index";
    }

    @PreAuthorize("@currentUserServiceImplementation.canSeeExercise(principal, #id)")
    @RequestMapping("/exercise/{id}")
    public String show(@PathVariable Long id, Model model){
        model.addAttribute("exercise", exerciseService.getExercise(id));
        return "exercises/show";
    }

    @RequestMapping("/create_exercise")
    public String create(Model model){
        model.addAttribute("exerciseForm", new ExerciseForm());
        model.addAttribute("userGroups", userGroupService.allUserGroups());
        return "exercises/create";
    }

    @RequestMapping(value = "/create_exercise",  method = RequestMethod.POST)
    public String store(@Valid ExerciseForm exerciseForm, BindingResult bindingResult, Model model){
        if (bindingResult.hasErrors()) {
            return "exercises/create";
        }
        Exercise exercise = exerciseService.saveExercise(exerciseForm);
        return "redirect:/exercise/" + exercise.getId();
    }

    @RequestMapping("/edit_exercise/{id}")
    public String edit(@PathVariable Long id, Model model) {
        model.addAttribute("id", id);
        ExerciseForm exerciseForm = new ExerciseForm(exerciseService.getExercise(id));
        model.addAttribute("exerciseForm", exerciseForm);
        model.addAttribute("userGroups", userGroupService.allUserGroups());
        return "exercises/edit";
    }

    @RequestMapping(value = "/edit_exercise/{id}", method = RequestMethod.POST)
    public String update(@PathVariable Long id, @Valid ExerciseForm exerciseForm, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("id", id);
            model.addAttribute("userGroups", userGroupService.allUserGroups());
            return "exercises/edit";
        }
        exerciseService.updateExercise(id, exerciseForm);
        return "redirect:/exercise/" + id;
    }

    @RequestMapping(value = "/delete_exercise/{id}", method = RequestMethod.POST)
    public String delete(@PathVariable Long id) {
        exerciseService.deleteExercise(id);
        return "redirect:/exercises";
    }


}
