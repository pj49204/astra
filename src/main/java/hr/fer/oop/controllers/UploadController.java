package hr.fer.oop.controllers;

import hr.fer.oop.entities.Assignment;
import hr.fer.oop.entities.Upload;
import hr.fer.oop.entities.UploadSourceEntry;
import hr.fer.oop.security.CustomUserDetails;
import hr.fer.oop.services.AssignmentService;
import hr.fer.oop.services.jpa.UploadServiceImplementation;
import hr.fer.oop.services.jpa.UploadSourceEntryServiceImplementation;
import hr.fer.oop.utils.unzip.FileUtils;
import hr.fer.oop.utils.unzip.UnzipFileResult;
import net.minidev.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;

@Controller
public class UploadController {

    private Environment env;

    private AssignmentService assignmentService;

    @Autowired
    public void setEnv (Environment env) {
        this.env = env;
    }

    @Autowired
    public void setAssignmentService (AssignmentService assignmentService) {
        this.assignmentService = assignmentService;
    }

    private UploadServiceImplementation uploadServiceImplementation;

    @Autowired
    public void setUploadServiceImplementation (UploadServiceImplementation uploadServiceImplementation) {
        this.uploadServiceImplementation = uploadServiceImplementation;
    }

    private UploadSourceEntryServiceImplementation uploadSourceEntryServiceImplementation;

    @Autowired
    public void setUploadSourceEntryServiceImplementation(UploadSourceEntryServiceImplementation uploadSourceEntryServiceImplementation) {
        this.uploadSourceEntryServiceImplementation = uploadSourceEntryServiceImplementation;
    }

    @RequestMapping("/upload/{id}")
    public String uploadFileHome(@PathVariable("id") Long id, Model model) {
        Assignment assignment = assignmentService.getAssignmentById(id);
        model.addAttribute("assignment", assignment);
        return "upload/upload";
    }

    @RequestMapping("/view_source/{uploadId}")
    public String viewSource(@PathVariable("uploadId") Long id, Model model) {
        model.addAttribute("upload", uploadServiceImplementation.getUploadById(id));
        return "upload/view_source";
    }

    @RequestMapping("/upload_source_entry/{uploadSourceEntryId}")
    public ResponseEntity<?> getUploadSourceEntry(@PathVariable("uploadSourceEntryId") Long id, Model model)  {
        UploadSourceEntry uploadSourceEntry = uploadSourceEntryServiceImplementation.getUploadSourceEntryById(id);
        JSONObject response = new JSONObject();
        try {
            String source = new String (Files.readAllBytes(Paths.get(uploadSourceEntry.getPath())), Charset.forName("UTF-8"));
            response.put("source", source);
        } catch (IOException e) {
            response.put("error", "There was an error while reading source file.");
            return new ResponseEntity<>(response, HttpStatus.OK);
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }


    @RequestMapping(value = "/upload/{assignmentId}", method = RequestMethod.POST)
    public ResponseEntity<?> uploadFile (@PathVariable("assignmentId") Long assignmentId, @RequestParam("runnableFile") MultipartFile runnableFile, @RequestParam("sourceFile") MultipartFile sourceFile) {
        JSONObject response = new JSONObject();
        CustomUserDetails user = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        try {

            String directory = String.format("%s/solution-%d-%d", env.getProperty("astra.uploadDirectory"), assignmentId, user.getUser().getId());
            FileUtils runnableFileUtils = new FileUtils(directory, "application/x-java-archive");
            FileUtils sourceFileUtils = new FileUtils(directory, "application/zip", "application/octet-stream");

            UnzipFileResult unzipFileResult = sourceFileUtils.unzipFile(sourceFile, "source");

            Upload uploadData = new Upload(runnableFileUtils.uploadFile(runnableFile, "runnable"), unzipFileResult);
            uploadData.setUser(user.getUser());

            Assignment assignment = assignmentService.getAssignmentById(assignmentId);
            uploadData.setAssignment(assignment);

            Upload upload = uploadServiceImplementation.saveUpload(uploadData);

            response.put("success", true);
            response.put("upload", upload);
        }
        catch (MaxUploadSizeExceededException e) {
            System.out.println(e.getMessage());
            response.put("error", "Max file size exceeded");
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
        catch (Exception e) {
            response.put("error", e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
