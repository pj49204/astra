package hr.fer.oop.controllers;

import hr.fer.oop.entities.Assignment;
import hr.fer.oop.entities.Exercise;
import hr.fer.oop.entities.TestResult;
import hr.fer.oop.entities.UserGroup;
import hr.fer.oop.repositories.ExerciseRepository;
import hr.fer.oop.security.CustomUserDetails;
import hr.fer.oop.services.AssignmentService;
import hr.fer.oop.services.ExerciseService;
import hr.fer.oop.services.TestResultService;
import hr.fer.oop.services.UserGroupService;
import hr.fer.oop.services.jpa.ExerciseServiceImplementation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Stream;

/**
 * Created by pavao on 04.11.16..
 */
@Controller
public class HomeController {

    private ExerciseServiceImplementation exerciseServiceImplementation;

    @Autowired
    public void setExerciseServiceImplementation (ExerciseServiceImplementation exerciseServiceImplementation) {
        this.exerciseServiceImplementation = exerciseServiceImplementation;
    }

    @RequestMapping({"/", "/home"})
    public String home(Model model){
        if(SecurityContextHolder.getContext().getAuthentication().getPrincipal() instanceof CustomUserDetails){
            List<Exercise> exercises = exerciseServiceImplementation.allCurrentExercisesForCurrentUser();
            model.addAttribute("exercises", exercises);
        }

        return "home";
    }
}
