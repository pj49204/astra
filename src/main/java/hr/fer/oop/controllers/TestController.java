package hr.fer.oop.controllers;

import hr.fer.oop.entities.TestFailure;
import hr.fer.oop.entities.TestResult;
import hr.fer.oop.entities.Upload;
import hr.fer.oop.repositories.UserRepository;
import hr.fer.oop.security.CustomUserDetails;
import hr.fer.oop.services.TestResultService;
import hr.fer.oop.services.UploadService;
import hr.fer.oop.testRunner.CustomTestExecutionSummary;
import hr.fer.oop.testRunner.TestRunner;
import net.minidev.json.JSONObject;
import org.junit.platform.launcher.listeners.TestExecutionSummary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Controller
public class TestController {

    // TODO: use setters
    private TestResultService testResultService;
    private UserRepository userRepository;
    private UploadService uploadService;

    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Autowired
    public void setTestResultService(TestResultService testResultService) {
        this.testResultService = testResultService;
    }

    @Autowired
    public void setUploadService(UploadService uploadService) {
        this.uploadService = uploadService;
    }


    private ThreadPoolTaskExecutor taskExecutor;

    @Autowired
    public void setTaskExecutor(ThreadPoolTaskExecutor taskExecutor) {
        this.taskExecutor = taskExecutor;
    }

    @PreAuthorize("@currentUserServiceImplementation.canStartTest(principal, #uploadId)")
    @RequestMapping(value = "/test/{uploadId}", method = RequestMethod.POST)
    public ResponseEntity<?> testStart(@PathVariable Long uploadId, Model model) {
        JSONObject response = new JSONObject();

        Upload upload = uploadService.getUploadById(uploadId);
        String classPath = upload.getRunnablePath();
        String testPath = upload.getAssignment().getTestsPath();
        CustomUserDetails user = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        TestRunner testRunner = new TestRunner(classPath, testPath);

        ListenableFuture<CustomTestExecutionSummary> future = taskExecutor.submitListenable(testRunner::start);

        future.addCallback(new ListenableFutureCallback<CustomTestExecutionSummary>() {
            @Override
            public void onFailure(Throwable throwable) {
                response.put("success", false);
            }

            @Override
            public void onSuccess(CustomTestExecutionSummary summary) {
                List<TestExecutionSummary.Failure> failures = summary.getFailures();
                List<TestFailure> testFailures = new ArrayList<>();
                for (CustomTestExecutionSummary.Failure failure : failures) {
                    String[] info = failure.getTestIdentifier().getSource().get().toString().split("\\'");
                    TestFailure testFailure = new TestFailure(
                            info[1], //Class name
                            info[3], //Method name
                            failure.getException().toString()
                    );
                    testFailures.add(testFailure);
                }
                TestResult testResult = new TestResult(
                        upload,
                        userRepository.findByUsername(user.getUsername()),
                        LocalDateTime.now(),
                        summary.getTestsStartedCount(),
                        summary.getTestsSucceededCount(),
                        summary.getTestsFailedCount(),
                        testFailures
                );
                testResultService.saveTestResult(testResult);

                response.put("success", true);
            }
        });
        response.put("assignment", upload.getAssignment());
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
