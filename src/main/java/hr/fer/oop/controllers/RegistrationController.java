package hr.fer.oop.controllers;

import hr.fer.oop.entities.User;
import hr.fer.oop.repositories.UserRepository;
import hr.fer.oop.security.CustomUserDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.Set;

/**
 * Created by pavao on 07.11.16..
 */
@Controller
public class RegistrationController {
    private UserRepository userRepository;
    @Autowired
    public void setUserRepository(UserRepository userRepository){
        this.userRepository=userRepository;
    }

    @Bean
    public PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }

    @RequestMapping(value = "/registration",method = RequestMethod.GET)
    public String registrationStart(){

        return "registration";
    }
    @RequestMapping(value = "/registration",method = RequestMethod.POST)
    public String registration(
            @RequestParam("username") String username,
            @RequestParam("password") String password,
            @RequestParam("firstName") String firstName,
            @RequestParam("lastName") String lastName,
            @RequestParam("JMBAG") String JMBAG,
            Model model){
        try{
            User user = new User(
                    username,
                    passwordEncoder().encode(password),
                    "USER",
                    firstName,
                    lastName,
                    JMBAG
            );
            if(userRepository.findByUsername(username)==null && password.length()>=8){
                userRepository.save(user);
                model.addAttribute("errorMessage","CONGRATULATIONS!!\n");
            }else if(password.length()<8){
                model.addAttribute("errorMessage","Field PASSWORD is too short\n");
            }else{
                model.addAttribute("errorMessage","Username is already in use\n");
            }
        }catch (ConstraintViolationException e){
            String errorMessage = "";
            System.out.println(e.getMessage());
            Set<ConstraintViolation<?>> constraintViolationSet= e.getConstraintViolations();
            for (ConstraintViolation<?> constraintViolation:
                 constraintViolationSet) {
                System.out.println(constraintViolation.getRootBeanClass().getSimpleName());
                System.out.println(constraintViolation.getPropertyPath());
                System.out.println(constraintViolation.getInvalidValue());
                errorMessage+=String.format("Field %s is incorrect\n", constraintViolation.getPropertyPath().toString().toUpperCase());
            }
            model.addAttribute("errorMessage",errorMessage);
        }
        return "registration";
    }
}
