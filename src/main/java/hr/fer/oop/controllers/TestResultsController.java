package hr.fer.oop.controllers;

import hr.fer.oop.entities.TestResult;
import hr.fer.oop.services.TestResultService;
import hr.fer.oop.services.auth.CurrentUserServiceImplementation;
import net.minidev.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class TestResultsController {

    private TestResultService testResultService;

    @Autowired
    public void setTestResultService (TestResultService testResultService) {
        this.testResultService = testResultService;
    }

    @PreAuthorize("@currentUserServiceImplementation.canAccessTestResult(principal, #id)")
    @RequestMapping(value="/test_results/{id}")
    public ResponseEntity<?> show (@PathVariable Long id) {
        TestResult testResult = testResultService.getTestResultById(id);
        return new ResponseEntity<>(testResult, HttpStatus.OK);
    }
}
