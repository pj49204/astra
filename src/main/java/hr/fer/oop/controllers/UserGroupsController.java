package hr.fer.oop.controllers;

import hr.fer.oop.entities.UserGroup;
import hr.fer.oop.forms.UserGroupForm;
import hr.fer.oop.services.UserGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;

@Controller
public class UserGroupsController {

    private UserGroupService userGroupService;

    @Autowired
    public void setUserGroupService (UserGroupService userGroupService) {
        this.userGroupService = userGroupService;
    }

    @RequestMapping("/user_groups")
    public String index(Model model){
        model.addAttribute("userGroups", userGroupService.allUserGroups());
        return "user_groups/index";
    }

    @RequestMapping("/user_group/{id}")
    public String show(@PathVariable Long id, Model model){
        model.addAttribute("userGroup", userGroupService.getUserGroup(id));
        return "user_groups/show";
    }

    @RequestMapping("/create_user_group")
    public String create(Model model){
        model.addAttribute("userGroupForm", new UserGroupForm());
        return "user_groups/create";
    }

    @RequestMapping(value = "/create_user_group",  method = RequestMethod.POST)
    public String store(@Valid UserGroupForm userGroupForm, BindingResult bindingResult, Model model){
        if (bindingResult.hasErrors()) {
            return "user_groups/create";
        }
        UserGroup userGroup = userGroupService.saveUserGroup(userGroupForm);
        return "redirect:/user_group/" + userGroup.getId();
    }

    @RequestMapping("/edit_user_group/{id}")
    public String edit(@PathVariable Long id, Model model) {
        model.addAttribute("id", id);
        model.addAttribute("userGroupForm", userGroupService.getUserGroup(id));
        return "user_groups/edit";
    }

    @RequestMapping(value = "/user_group/{id}", method = RequestMethod.POST)
    public String update(@PathVariable Long id, @Valid UserGroupForm userGroupForm, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("id", id);
            return "user_groups/edit";
        }
        userGroupService.updateUserGroup(id, userGroupForm);
        return "redirect:/user_group/" + id;
    }

    @RequestMapping(value = "/delete_user_group/{id}", method = RequestMethod.POST)
    public String delete(@PathVariable Long id) {
        userGroupService.deleteUserGroup(id);
        return "redirect:/user_groups";
    }

    @RequestMapping(value = "/delete_user_user_group/{userGroupId}/{userId}", method = RequestMethod.POST)
    public String removeFromUserGroup (@PathVariable("userGroupId") Long userGroupid, @PathVariable("userId") Long userId, Model model) {
        userGroupService.removeUserFromUserGroup(userId, userGroupid);
        return "redirect:/user_group/" + userGroupid;
    }


}
