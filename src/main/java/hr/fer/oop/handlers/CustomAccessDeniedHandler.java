package hr.fer.oop.handlers;

import hr.fer.oop.entities.User;
import hr.fer.oop.repositories.UserRepository;
import hr.fer.oop.security.CustomUserDetails;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collection;
import java.util.LinkedList;

/**
 * Created by pavao on 10.11.16..
 */
@Component
public class CustomAccessDeniedHandler implements AccessDeniedHandler {

    private String errorPage;

    public String getErrorPage() {
        return errorPage;
    }

    public void setErrorPage(String errorPage) {
        this.errorPage = errorPage;
    }


    @Override
    public void handle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, AccessDeniedException e) throws IOException, ServletException {
        System.out.println(String.format("Page: \"%s\", User:  %s, %s",
                httpServletRequest.getServletPath(),
                httpServletRequest.getRemoteUser(),
                e.getLocalizedMessage()
            ));
        if(httpServletRequest.getRemoteUser()==null){
            httpServletResponse.sendRedirect("/login");
            return;
        }

        CustomUserDetails user =  (CustomUserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(user==null){
            setErrorPage("/login");
        }else{
            if(user.getAuthorities()
                    .stream()
                    .anyMatch(grantedAuthority -> grantedAuthority.getAuthority().equals("ADMIN"))){
                setErrorPage("/"); //todo: tu ide admin panel;
            }else{
                setErrorPage("/");
            }
        }

        httpServletResponse.sendRedirect(getErrorPage());
    }
}
