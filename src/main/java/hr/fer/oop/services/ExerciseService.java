package hr.fer.oop.services;

import hr.fer.oop.entities.Exercise;
import hr.fer.oop.forms.ExerciseForm;

import java.util.Iterator;
import java.util.List;

/**
 * Created by pavao on 10.01.17..
 */
public interface ExerciseService {
    Iterable<Exercise> allExercises();
    Iterable<Exercise> allCurrentExercises();
    List<Exercise> allCurrentExercisesForCurrentUser();
    Exercise getExercise (Long id);
    Exercise getExercise (String string);
    Exercise saveExercise(ExerciseForm exerciseForm);
    Exercise saveExercise(Exercise exercise);
    Exercise updateExercise (Long id, ExerciseForm exercise);
    void deleteExercise(Long id);

}
