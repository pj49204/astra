package hr.fer.oop.services;

import hr.fer.oop.entities.User;
import hr.fer.oop.security.CustomUserDetails;
import org.springframework.stereotype.Service;

public interface CurrentUserService {
    boolean canAccessTestResult (CustomUserDetails user, Long id);
    boolean canStartTest (CustomUserDetails user, Long id);
    boolean canSeeUser (CustomUserDetails user, Long id);
    boolean canSeeAssignment (CustomUserDetails user, Long id);
    boolean canSeeExercise (CustomUserDetails user, Long id);
}
