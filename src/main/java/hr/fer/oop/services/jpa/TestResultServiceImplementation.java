package hr.fer.oop.services.jpa;

import hr.fer.oop.entities.TestFailure;
import hr.fer.oop.entities.TestResult;
import hr.fer.oop.repositories.TestFailureRepository;
import hr.fer.oop.repositories.TestResultRepository;
import hr.fer.oop.services.TestResultService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TestResultServiceImplementation implements TestResultService {

    private TestResultRepository testResultRepository;
    private TestFailureRepository testFailureRepository;

    @Autowired
    public void setTestResultRepository (TestResultRepository testResultRepository) {
        this.testResultRepository = testResultRepository;
    }

    @Autowired
    public void setTestFailureRepository (TestFailureRepository testFailureRepository) {
        this.testFailureRepository = testFailureRepository;
    }

    @Override
    public Iterable<TestResult> allTestResults() {
        return testResultRepository.findAll();
    }

    @Override
    public TestResult getTestResultById(Long id) {
        return testResultRepository.findOne(id);
    }

    @Override
    public TestResult saveTestResult(TestResult testResult) {
        TestResult testResultDb;
        try{
            testResultDb = testResultRepository.save(testResult);
            for(TestFailure testFailure : testResult.getTestFailures()) {
                testFailureRepository.save(new TestFailure(testFailure, testResultDb));
            }
        }catch (Exception e){
            testResultDb = testResultRepository.findByUploadId(testResult.getUpload().getId());
            for(TestFailure testFailure : testResultDb.getTestFailures()){
                testFailureRepository.delete(testFailure);
            }
            testResultRepository.delete(testResultDb);
            testResultDb = testResultRepository.save(testResult);
            for(TestFailure testFailure : testResult.getTestFailures()) {
                testFailureRepository.save(new TestFailure(testFailure, testResultDb));
            }
        }
        return testResultDb;
    }

    @Override
    public void deleteTestResult(Long id) {
        testResultRepository.delete(id);
    }
}
