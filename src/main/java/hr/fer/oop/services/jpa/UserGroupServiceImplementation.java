package hr.fer.oop.services.jpa;

import hr.fer.oop.entities.Exercise;
import hr.fer.oop.entities.User;
import hr.fer.oop.entities.UserGroup;
import hr.fer.oop.forms.UserGroupForm;
import hr.fer.oop.repositories.ExerciseRepository;
import hr.fer.oop.repositories.UserGroupRepository;
import hr.fer.oop.repositories.UserRepository;
import hr.fer.oop.services.ExerciseService;
import hr.fer.oop.services.UserGroupService;
import hr.fer.oop.services.UserService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by pavao on 10.01.17..
 */
@Service
public class UserGroupServiceImplementation implements UserGroupService {

    private UserGroupRepository userGroupRepository;

    @Autowired
    public void setUserGroupRepository(UserGroupRepository userGroupRepository) {
        this.userGroupRepository = userGroupRepository;
    }

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private ExerciseRepository exerciseRepository;

    private ExerciseService exerciseService;

    @Autowired
    public void setExerciseService(ExerciseServiceImplementation exerciseService) {
        this.exerciseService = exerciseService;
    }

    private UserService userService;

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Override
    public Iterable<User> allUsers() {
        return null;
    }

    @Override
    public Iterable<Exercise> allExercises() {
        return null;
    }

    @Override
    public Iterable<UserGroup> allUserGroups() {
        return userGroupRepository.findAll();
    }

    @Override
    public Exercise getExerciseById(Long id) {
        return null;
    }

    @Override
    public User insertUserToUserGroup(User user) {
        return null;
    }

    @Override
    public Exercise insertExerciseToUserGroup(Exercise exercise) {
        return null;
    }

    @Override
    public UserGroup removeUserFromUserGroup(Long userId, Long userGroupId) {
        UserGroup userGroup = getUserGroup(userGroupId);
        User user = userService.getUser(userId);
        if (user.getUserGroups().size() <= 1) {
            userGroup.getUsers().removeIf(user::equals);
            if(!user.getRole().equals("ADMIN")) userService.deleteUser(userId);
            else user.getUserGroups().removeIf(userGroup::equals);
        } else {
            user.getUserGroups().removeIf(userGroup::equals);
            userGroup.getUsers().removeIf(user::equals);
        }
        return userGroupRepository.save(userGroup);

    }

    @Override
    public UserGroup removeUserFromUserGroup(String name, UserGroup userGroup) {
        User user = userRepository.findByUsername(name);
        if (user.getUserGroups().size() <= 1) {
            userGroup.getUsers().removeIf(user::equals);
            userService.deleteUser(user.getId());
        } else {
            userGroup.getUsers().removeIf(user::equals);
            user.getUserGroups().removeIf(userGroup::equals);
        }
        return userGroupRepository.save(userGroup);
    }

    @Override
    public UserGroup removeExerciseFromUserGroup(Long id, UserGroup userGroup) {
        UserGroup userGroupDB = userGroupRepository.findByName(userGroup.getName());
        userGroupDB.getExercises().remove(exerciseService.getExercise(id));
        BeanUtils.copyProperties(userGroupDB, userGroup);
        return userGroupRepository.save(userGroup);
    }

    @Override
    public UserGroup removeExerciseFromUserGroup(String name, UserGroup userGroup) {
        UserGroup userGroupDB = userGroupRepository.findByName(userGroup.getName());
        userGroupDB.getExercises().remove(exerciseService.getExercise(name));
        BeanUtils.copyProperties(userGroupDB, userGroup);
        return userGroupRepository.save(userGroup);
    }


    @Override
    public UserGroup saveUserGroup(UserGroupForm userGroupForm) {
        UserGroup userGroup = new UserGroup(userGroupForm);
        UserGroup userGroupDB = userGroupRepository.save(userGroup);
        Set<User> users = userGroupDB.getUsers();
        Set<Exercise> exercises = userGroupDB.getExercises();
        for (User user : users) {
            User userDB = userRepository.findByUsername(user.getUsername());
            System.out.println(userDB.getUserGroups());
            userDB.getUserGroups().add(userGroup);
            userRepository.save(userDB);
        }
        for (Exercise exercise : exercises) {
            Exercise exerciseDB = exerciseRepository.findByName(exercise.getName());
            exerciseDB.getUserGroups().add(userGroup);
            exerciseRepository.save(exerciseDB);
        }
        return userGroupDB;
    }

    @Override
    public UserGroup updateUserGroup(Long id, UserGroupForm userGroupForm) {
        return null;
    }

    @Override
    public UserGroup getUserGroup(String name) {
        return userGroupRepository.findByName(name);
    }

    @Override
    public UserGroup getUserGroup(Long id) {
        return userGroupRepository.findOne(id);
    }

    @Override
    public void deleteUserGroup(Long id) {
        Set<Long> userIds = getUserGroup(id).getUsers()
                .stream()
                .filter(user -> !user.getRole().equals("ADMIN"))
                .map(User::getId)
                .collect(Collectors.toSet());

        userGroupRepository.delete(id);

        userIds
        .forEach(userRepository::delete);
    }
}
