package hr.fer.oop.services.jpa;

import hr.fer.oop.entities.Exercise;
import hr.fer.oop.entities.User;
import hr.fer.oop.entities.UserGroup;
import hr.fer.oop.forms.ExerciseForm;
import hr.fer.oop.repositories.AssignmentRepository;
import hr.fer.oop.repositories.ExerciseRepository;
import hr.fer.oop.repositories.UserRepository;
import hr.fer.oop.security.CustomUserDetails;
import hr.fer.oop.services.AssignmentService;
import hr.fer.oop.services.ExerciseService;
import hr.fer.oop.services.UserGroupService;
import org.hibernate.Hibernate;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created by pavao on 10.01.17..
 */
@Service
public class ExerciseServiceImplementation implements ExerciseService {


    private ExerciseRepository exerciseRepository;

    @Autowired
    public void setExerciseRepository (ExerciseRepository exerciseRepository) {
        this.exerciseRepository = exerciseRepository;
    }

    private UserRepository userRepository;

    @Autowired
    public void setUserRepository (UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    private UserGroupService userGroupService;
    @Autowired
    public void setAssignmentService(UserGroupService userGroupService){
        this.userGroupService = userGroupService;
    }

    @Override
    public Iterable<Exercise> allExercises() {
        return exerciseRepository.findAll();
    }

    @Override
    public Iterable<Exercise> allCurrentExercises() {
        return exerciseRepository.findAllCurrent();
    }

    @Override
    public List<Exercise> allCurrentExercisesForCurrentUser() {
        CustomUserDetails userDetails = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = userRepository.findOne(userDetails.getUser().getId());
        List<Exercise> exercises = new ArrayList<>();
        Set<UserGroup> userGroups = user.getUserGroups();
        if (userGroups != null) {
            for (UserGroup g : userGroups) {
                exerciseRepository.findAllCurrentByUserGroupId(g.getId()).forEach(exercises::add);
            }
        }
        return exercises;
    }

    @Override
    public Exercise getExercise(Long id) {
        return exerciseRepository.findOne(id);
    }

    @Override
    public Exercise getExercise(String name) {
        return exerciseRepository.findByName(name);
    }

    @Override
    public Exercise saveExercise(ExerciseForm exerciseForm) {
        Exercise[] exercises = new Exercise[1];
        exercises[0] = new Exercise(exerciseForm);
        exerciseForm.getUserGroupIds().forEach(id -> {
            UserGroup userGroup = userGroupService.getUserGroup(id);
            userGroup.getExercises().add(exercises[0]);
            exercises[0].getUserGroups().add(userGroup);
        });
        return exerciseRepository.save(exercises[0]);
    }

    @Override
    public Exercise saveExercise(Exercise exercise) {
        return exerciseRepository.save(exercise);
    }

    @Override
    public Exercise updateExercise(Long id, ExerciseForm exerciseForm) {
        Exercise[] exercises = new Exercise[1];
        exercises[0] = exerciseRepository.findOne(id);
        BeanUtils.copyProperties(exerciseForm, exercises[0]);

        exercises[0].getUserGroups().forEach(userGroup -> {
            userGroup.getExercises().removeIf(exercise -> exercise.getId().equals(exercises[0].getId()));
        });

        exerciseForm.getUserGroupIds().forEach(userGroupId -> {
            UserGroup userGroup = userGroupService.getUserGroup(userGroupId);
            userGroup.getExercises().add(exercises[0]);
            exercises[0].getUserGroups().add(userGroup);
        });
        return exerciseRepository.save(exercises[0]);
    }

    @Override
    public void deleteExercise(Long id) {
        Exercise exercise = exerciseRepository.getOne(id);
        exercise.getUserGroups().forEach(userGroup ->
                userGroup.getExercises().removeIf(exercise::equals)
        );
        exerciseRepository.delete(id);
    }
}
