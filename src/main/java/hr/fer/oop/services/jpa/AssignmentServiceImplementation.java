package hr.fer.oop.services.jpa;

import hr.fer.oop.entities.Assignment;
import hr.fer.oop.entities.Exercise;
import hr.fer.oop.forms.AssignmentForm;
import hr.fer.oop.repositories.AssignmentRepository;
import hr.fer.oop.repositories.ExerciseRepository;
import hr.fer.oop.services.AssignmentService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

@Service
public class AssignmentServiceImplementation implements AssignmentService {

    private AssignmentRepository assignmentRepository;

    @Autowired
    public void setAssignmentRepository (AssignmentRepository assignmentRepository) {
        this.assignmentRepository = assignmentRepository;
    }

    @Autowired
    private ExerciseRepository exerciseRepository;


    public void setExerciseRepository (ExerciseRepository exerciseRepository) {
        this.exerciseRepository = exerciseRepository;
    }


    private EntityManager entityManager;

    @Autowired
    private EntityManagerFactory entityManagerFactory;

//    @Autowired
//    public void setEntityManager(@Qualifier("exerciseEntityManager") EntityManager entityManager) {
//        this.entityManager = entityManagerFactory.;
//    }


    @Override
    public Iterable<Assignment> allAssignments() {
        return assignmentRepository.findAll();
    }

    @Override
    public Assignment getAssignmentById(Long id) {
        return assignmentRepository.findOne(id);
    }

    @Override
    public Assignment saveAssignment(AssignmentForm assignmentForm) {
        Assignment assignment = new Assignment(assignmentForm);
        Exercise exercise = exerciseRepository.getOne(assignmentForm.getExerciseId());
        assignment.setExercise(exercise);
        return assignmentRepository.save(assignment);
    }

    @Override
    public Assignment updateAssignment(Long id, AssignmentForm assignmentForm) {
        Assignment assignment = getAssignmentById(id);
        BeanUtils.copyProperties(assignmentForm, assignment);
        assignment.setExercise(exerciseRepository.getOne(assignmentForm.getExerciseId()));
        return assignmentRepository.save(assignment);
    }

    @Override
    public void deleteAssignment(Long id) {
        assignmentRepository.delete(id);
    }
}

