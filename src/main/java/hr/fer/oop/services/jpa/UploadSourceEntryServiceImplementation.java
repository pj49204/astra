package hr.fer.oop.services.jpa;

import hr.fer.oop.entities.UploadSourceEntry;
import hr.fer.oop.repositories.ExerciseRepository;
import hr.fer.oop.repositories.UploadSourceEntryRepository;
import hr.fer.oop.services.UploadSourceEntryService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UploadSourceEntryServiceImplementation implements UploadSourceEntryService {

    private UploadSourceEntryRepository uploadSourceEntryRepository;

    @Autowired
    public void setUploadSourceEntryRepository (UploadSourceEntryRepository uploadSourceEntryRepository) {
        this.uploadSourceEntryRepository = uploadSourceEntryRepository;
    }

    @Override
    public Iterable<UploadSourceEntry> allUploadSourceEntrys() {
        return uploadSourceEntryRepository.findAll();
    }

    @Override
    public UploadSourceEntry getUploadSourceEntryById(Long id) {
        return uploadSourceEntryRepository.findOne(id);
    }

    @Override
    public UploadSourceEntry saveUploadSourceEntry(UploadSourceEntry uploadSourceEntry) {
        return uploadSourceEntryRepository.save(uploadSourceEntry);
    }

    @Override
    public UploadSourceEntry updateUploadSourceEntry(Long id, UploadSourceEntry uploadSourceEntry) {
        UploadSourceEntry uploadSourceEntryDB = getUploadSourceEntryById(id);
        BeanUtils.copyProperties(uploadSourceEntry, uploadSourceEntryDB);
        return uploadSourceEntryRepository.save(uploadSourceEntry);
    }

    @Override
    public void deleteUploadSourceEntry(Long id) {
        uploadSourceEntryRepository.delete(id);
    }
}

