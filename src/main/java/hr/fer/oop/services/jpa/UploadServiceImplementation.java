package hr.fer.oop.services.jpa;

import hr.fer.oop.entities.Upload;
import hr.fer.oop.entities.UploadSourceEntry;
import hr.fer.oop.repositories.UploadRepository;
import hr.fer.oop.repositories.ExerciseRepository;
import hr.fer.oop.repositories.UploadSourceEntryRepository;
import hr.fer.oop.services.UploadService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UploadServiceImplementation implements UploadService {

    private UploadRepository uploadRepository;

    @Autowired
    public void setUploadRepository (UploadRepository uploadRepository) {
        this.uploadRepository = uploadRepository;
    }

    private ExerciseRepository exerciseRepository;

    @Autowired
    public void setExerciseRepository (ExerciseRepository exerciseRepository) {
        this.exerciseRepository = exerciseRepository;
    }

    private UploadSourceEntryRepository uploadSourceEntryRepository;

    @Autowired
    public void setUploadSourceEntryRepository(UploadSourceEntryRepository uploadSourceEntryRepository) {
        this.uploadSourceEntryRepository = uploadSourceEntryRepository;
    }

    @Override
    public Iterable<Upload> allUploads() {
        return uploadRepository.findAll();
    }

    @Override
    public Upload getUploadById(Long id) {
        return uploadRepository.findOne(id);
    }

    @Override
    public Upload saveUpload(Upload upload) {
        Upload uploadDB = uploadRepository.save(upload);
        uploadDB.getUploadSourceEntries()
                .forEach(uploadSourceEntry -> {
                    uploadSourceEntry.setUpload(uploadDB);
                    uploadSourceEntryRepository.save(uploadSourceEntry);
                });
        return uploadDB;
    }

    @Override
    public Upload updateUpload(Long id, Upload upload) {
        Upload uploadDB = getUploadById(id);
        BeanUtils.copyProperties(upload, uploadDB);
        return uploadRepository.save(upload);
    }

    @Override
    public void deleteUpload(Long id) {
        uploadRepository.delete(id);
    }
}

