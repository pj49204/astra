package hr.fer.oop.services.jpa;

import hr.fer.oop.entities.User;
import hr.fer.oop.entities.UserGroup;
import hr.fer.oop.forms.UserForm;
import hr.fer.oop.repositories.AssignmentRepository;
import hr.fer.oop.repositories.UserRepository;
import hr.fer.oop.services.UserGroupService;
import hr.fer.oop.services.UserService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

/**
 * Created by pavao on 10.01.17..
 */
@Service
public class UserServiceImplementation implements UserService {

    private UserRepository userRepository;


    private PasswordEncoder passwordEncoder;
    @Autowired
    public void setPasswordEncoder(PasswordEncoder passwordEncoder){
        this.passwordEncoder= new BCryptPasswordEncoder();
    }

    @Autowired
    public void setUserRepository (UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    private UserGroupService userGroupService;
    @Autowired
    public void setAssignmentService(UserGroupService userGroupService){
        this.userGroupService = userGroupService;
    }

    @Override
    public Iterable<User> allUsers() {
        return userRepository.findAll();
    }

    @Override
    public User getUser(Long id) {
        return userRepository.findOne(id);
    }

    @Override
    public User saveUser(UserForm userForm) {
        User[] users = new User[1];
        userForm.setPassword(passwordEncoder.encode(userForm.getPassword()));
        users[0] = new User(userForm);
        userForm.getUserGroupIds().forEach(userGroupId ->{
            UserGroup userGroupDb = userGroupService.getUserGroup(userGroupId);
            userGroupDb.getUsers().add(users[0]);
            users[0].getUserGroups().add(userGroupDb);
        });
        return userRepository.save(users[0]);
    }

    @Override
    public User updateUser(Long id, UserForm userForm) {
        User[] users = new User[1];
        users[0] = userRepository.findOne(id);
        // We presume that user's password can't be changed
        // userForm.setPassword(passwordEncoder.encode(userForm.getPassword()));
        users[0].getUserGroups().forEach(userGroup -> {
            userGroup.getUsers().removeIf(user -> user.getId() == users[0].getId());
        });
        if(userForm.getPassword() != "") {
            BeanUtils.copyProperties(userForm, users[0]);
            users[0].setPassword(passwordEncoder.encode(users[0].getPassword()));
        }else{
            String password = users[0].getPassword();
            BeanUtils.copyProperties(userForm, users[0]);
            users[0].setPassword(password);
        }

        userForm.getUserGroupIds().forEach(userGroupId -> {
            UserGroup userGroup = userGroupService.getUserGroup(userGroupId);
            userGroup.getUsers().add(users[0]);
            users[0].getUserGroups().add(userGroup);
        });
        return userRepository.save(users[0]);
    }

    @Override
    public List<User> saveUserList(List<User> users){
        return  userRepository.save(
                users.stream()
                    .map(user -> {
                        user.setPassword(passwordEncoder.encode(user.getPassword()));
                        return user;
                    })
                    .collect(Collectors.toList())
        );
    }

    @Override
    public void deleteUser(Long id) {
        User user = userRepository.getOne(id);
        user.getUserGroups().forEach(userGroup ->
            userGroup.getUsers().removeIf(user::equals)
        );
        userRepository.delete(id);
    }
}
