package hr.fer.oop.services;

import hr.fer.oop.entities.UploadSourceEntry;

/**
 * Created by ilakovac on 13.01.17..
 */
public interface UploadSourceEntryService {
    Iterable<UploadSourceEntry> allUploadSourceEntrys();
    UploadSourceEntry getUploadSourceEntryById(Long id);
    UploadSourceEntry saveUploadSourceEntry(UploadSourceEntry uploadSourceEntry);
    UploadSourceEntry updateUploadSourceEntry(Long id, UploadSourceEntry uploadSourceEntry);
    void deleteUploadSourceEntry(Long id);
}
