package hr.fer.oop.services;


import hr.fer.oop.entities.TestResult;

public interface TestResultService {
    Iterable<TestResult> allTestResults();
    TestResult getTestResultById (Long id);
    TestResult saveTestResult (TestResult testResult);
    void deleteTestResult(Long id);
}
