package hr.fer.oop.services;

import hr.fer.oop.entities.Upload;

/**
 * Created by ilakovac on 13.01.17..
 */
public interface UploadService {
    Iterable<Upload> allUploads();
    Upload getUploadById (Long id);
    Upload saveUpload(Upload upload);
    Upload updateUpload (Long id, Upload upload);
    void deleteUpload(Long id);
}
