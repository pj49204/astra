package hr.fer.oop.services.auth;

import hr.fer.oop.entities.Assignment;
import hr.fer.oop.entities.Exercise;
import hr.fer.oop.entities.UserGroup;
import hr.fer.oop.security.CustomUserDetails;
import hr.fer.oop.services.AssignmentService;
import hr.fer.oop.services.CurrentUserService;
import hr.fer.oop.services.ExerciseService;
import hr.fer.oop.services.TestResultService;
import hr.fer.oop.services.jpa.AssignmentServiceImplementation;

import hr.fer.oop.services.jpa.ExerciseServiceImplementation;
import hr.fer.oop.services.jpa.TestResultServiceImplementation;
import hr.fer.oop.services.jpa.UploadServiceImplementation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;
import java.util.stream.Collectors;

@Service
public class CurrentUserServiceImplementation implements CurrentUserService {
    private TestResultService testResultService;
    @Autowired
    public void setTestResultService(TestResultServiceImplementation testResultService){
        this.testResultService = testResultService;
    }

    private AssignmentService assignmentService;
    @Autowired
    public void setAssignmentService(AssignmentServiceImplementation assignmentService){
        this.assignmentService = assignmentService;
    }

    private ExerciseService exerciseService;
    @Autowired
    public void setExerciseService(ExerciseServiceImplementation exerciseService){
        this.exerciseService = exerciseService;
    }

    private UploadServiceImplementation uploadServiceImplementation;
    @Autowired
    public void setUploadServiceImplementation (UploadServiceImplementation uploadServiceImplementation) {
        this.uploadServiceImplementation = uploadServiceImplementation;
    }

    private ExerciseServiceImplementation exerciseServiceImplementation;

    @Autowired
    public void setExerciseServiceImplementation (ExerciseServiceImplementation exerciseServiceImplementation) {
        this.exerciseServiceImplementation = exerciseServiceImplementation;
    }

    @Override
    public boolean canAccessTestResult(CustomUserDetails user, Long id) {
        return user.getUser().getRole().equals("ADMIN") || user.getUser().getId().equals(testResultService.getTestResultById(id).getUser().getId());
    }

    @Override
    public boolean canStartTest(CustomUserDetails user, Long id) {
        return uploadServiceImplementation.getUploadById(id).getUser().getId() == user.getUser().getId();
    }

    @Override
    public boolean canSeeUser(CustomUserDetails user, Long id) {
        return user.getUser().getRole().equals("ADMIN") || user.getUser().getId().equals(id);

    }

    @Override
    public boolean canSeeAssignment(CustomUserDetails user, Long id) {
        return user.getUser().getRole().equals("ADMIN") ||
                assignmentService.getAssignmentById(id).getExercise()
                .getUserGroups()
                .stream()
                .anyMatch(userGroup -> user.getUser().getUserGroups()
                        .stream()
                        .anyMatch(userGroupInner -> userGroup.getId().equals(userGroupInner.getId()))
                );
    }
    public boolean canSeeExercise(CustomUserDetails user, Long id) {
        System.out.println(exerciseServiceImplementation.getExercise(id).getUserGroups());
        System.out.println( user.getUser().getUserGroups());
        return user.getUser().getRole().equals("ADMIN") ||
                exerciseServiceImplementation.getExercise(id)
                .getUserGroups()
                .stream()
                .anyMatch(userGroup -> user.getUser().getUserGroups()
                    .stream()
                    .anyMatch(userGroupInner -> userGroup.getId().equals(userGroupInner.getId()))
                );
    }
}
