package hr.fer.oop.services;

import hr.fer.oop.entities.User;
import hr.fer.oop.forms.UserForm;

import java.util.List;

/**
 * Created by pavao on 10.01.17..
 */
public interface UserService {
    Iterable<User> allUsers();
    User getUser(Long id);
    User saveUser(UserForm userForm);
    List<User> saveUserList(List<User> users);
    User updateUser(Long id, UserForm user);
    void deleteUser(Long id);

}
