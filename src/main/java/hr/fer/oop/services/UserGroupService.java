package hr.fer.oop.services;

import hr.fer.oop.entities.Exercise;
import hr.fer.oop.entities.User;
import hr.fer.oop.entities.UserGroup;
import hr.fer.oop.forms.UserGroupForm;

/**
 * Created by pavao on 10.01.17..
 */
public interface UserGroupService {
    Iterable<User> allUsers();
    Iterable<Exercise> allExercises();
    Iterable<UserGroup> allUserGroups();
    Exercise getExerciseById(Long id);
    User insertUserToUserGroup(User user);
    Exercise insertExerciseToUserGroup(Exercise exercise);
    UserGroup removeUserFromUserGroup(Long userId, Long userGroupId);
    UserGroup removeUserFromUserGroup(String name, UserGroup userGroup);
    UserGroup removeExerciseFromUserGroup(Long id, UserGroup userGroup);
    UserGroup removeExerciseFromUserGroup(String name, UserGroup userGroup);
    UserGroup saveUserGroup(UserGroupForm userGroupForm);
    UserGroup updateUserGroup(Long id, UserGroupForm userGroupForm);
    UserGroup getUserGroup(String name);
    UserGroup getUserGroup(Long id);
    void deleteUserGroup (Long id);
}
