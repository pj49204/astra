package hr.fer.oop.services;


import hr.fer.oop.entities.Assignment;
import hr.fer.oop.forms.AssignmentForm;

public interface AssignmentService {
    Iterable<Assignment> allAssignments();
    Assignment getAssignmentById (Long id);
    Assignment saveAssignment(AssignmentForm assignment);
    Assignment updateAssignment (Long id, AssignmentForm assignment);
    void deleteAssignment(Long id);
}
