package hr.fer.oop.services;

import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * Created by pavao on 07.11.16..
 */
public interface LoginService extends UserDetailsService {
    boolean checkUsernameAndPassword(String username,String password);
}
