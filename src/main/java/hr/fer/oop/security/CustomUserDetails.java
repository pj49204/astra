package hr.fer.oop.security;

import hr.fer.oop.entities.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

/**
 * Implementation of {@link UserDetails}
 * Contains user information and user's authority list
 * Created by pavao on 07.11.16..
 */
public class CustomUserDetails implements UserDetails {
    private User user;

    /**
     * @return current user
     */
    public User getUser() {
        return user;
    }

    /**
     * String representation of current user details
     * @return string containing user's username and authority list
     */
    @Override
    public String toString() {
        return "CustomUserDetails{" +
                "user=" + user +
                ", authorityList=" + authorityList +
                '}';
    }

    private class CustomGrantedAuthority implements GrantedAuthority{
        private String authority;
        private CustomGrantedAuthority(String authority){
            this.authority=authority;
        }

        @Override
        public String getAuthority() {
            return authority;
        }
    }

    private List<CustomGrantedAuthority> authorityList;

    /**
     * Constructor that creates user details from given user
     * creates new authority list and adds given user's role to it
     * @param user given user
     */
    public CustomUserDetails(User user){
        this.user=user;
        authorityList=new LinkedList<>();
        authorityList.add(new CustomGrantedAuthority(user.getRole()));
    }

    /**
     * @return user's authority list
     */
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorityList;
    }

    /**
     * @return user's password
     */
    @Override
    public String getPassword() {
        return user.getPassword();
    }

    /**
     * @return user's username
     */
    @Override
    public String getUsername() {
        return user.getUsername();
    }

    /**
     * @return always true
     */
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    /**
     * @return always true
     */
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    /**
     * @return always true
     */
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    /**
     * @return always true
     */
    @Override
    public boolean isEnabled() {
        return true;
    }

    /**
     * @return user's Id
     */
    public Long getId () { return user.getId(); }

    /**
     * @return user's full name
     */
    public String getFullName () { return user.getFullName(); }
}
