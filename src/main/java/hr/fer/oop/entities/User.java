package hr.fer.oop.entities;

import hr.fer.oop.forms.UserForm;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.annotations.SQLInsert;
import org.springframework.beans.BeanUtils;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.*;

@Entity
@Table(name="\"User\"")
public class User {
    @Id
    @GeneratedValue
    private Long id;

    @NotNull
    @Column(unique=true)
    @Size(max=255,min = 1)
    private String username;

    @NotNull
    @Size(max=255,min = 8)
    private String password;

    @NotNull
    @Size(max=255,min = 1)
    private String role;

    @NotNull
    @Size(max=255,min=1)
    private String firstName;

    @NotNull
    @Size(max=255,min=1)
    private String lastName;

    @NotNull
    @Size(max=12,min=10)
    private String JMBAG;

    @ManyToMany(mappedBy = "users", cascade = CascadeType.DETACH, fetch = FetchType.EAGER)
    private Set<UserGroup> userGroups;

    @OneToMany(mappedBy = "user",cascade = CascadeType.ALL)
    private Set<Upload> uploads;

    public static List<Role> availableRoles = Arrays.asList(
            new Role("Admin", "ADMIN"),
            new Role("User", "USER")
    );

    private static class Role {
        private String name;
        private String value;

        public Role (String name, String value) {
            this.name = name;
            this.value = value;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }

    public Set<UserGroup> getUserGroups() {
        return userGroups;
    }

    public void setUserGroups(Set<UserGroup> userGroups) {
        this.userGroups = userGroups;
    }

    private void addUserGroup(UserGroup userGroup){
    }

    public User(){

    }


    public Long getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getRole() {
        return role;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getJMBAG() {
        return JMBAG;
    }

    public String getFullName () {
        return this.firstName + " " + this.lastName;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setJMBAG(String JMBAG) {
        this.JMBAG = JMBAG;
    }

    public Set<Upload> getUploads() {
        return uploads;
    }

    public void setUploads(Set<Upload> uploads) {
        this.uploads = uploads;
    }

    //for dev purposes, to be deleted upon publication
    public User(String username, String password, String role) {
        this(username,password,role,"FirstName", "LastName", "0000000000");
    }

    public User(String username, String password, String role, String firstName, String lastName, String JMBAG) {
        this(username,password,role,firstName,lastName,JMBAG,new HashSet<UserGroup>(), new HashSet<Upload>());
    }

    public User(User user, UserGroup userGroup){
        this(user.username,user.password,user.role,user.firstName,user.lastName,user.JMBAG);
        user.addUserGroup(userGroup);
    }

    public User (UserForm userForm) {
        BeanUtils.copyProperties(userForm, this);
        this.userGroups = new HashSet<>();
    }


    public User(String username, String password, String role, String firstName, String lastName, String JMBAG, Set<UserGroup> userGroups, Set<Upload> uploads) {
        this.username = username;
        this.password = password;
        this.role = role;
        this.firstName = firstName;
        this.lastName = lastName;
        this.JMBAG = JMBAG;
        this.userGroups = userGroups;
        this.uploads = uploads;
    }

    @Override
    public String toString() {
        return username + " (" + JMBAG + ")";
    }


}
