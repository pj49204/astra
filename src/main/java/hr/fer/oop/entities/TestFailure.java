package hr.fer.oop.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import hr.fer.oop.utils.parser.Parser;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.IOException;

/**
 * Created by pavao on 21.12.16..
 */
@Entity
public class TestFailure {
    @Id
    @GeneratedValue
    private Long testFailureId;

    @NotNull
    private String testClass;

    @NotNull
    private String testName;

    @NotNull
    private String testMessage;

    @ManyToOne
    @JoinColumn(name = "test_result_id")
    @JsonBackReference
    private TestResult testResult;

    public Long getTestFailureId() {
        return testFailureId;
    }

    public String getTestClass() {
        return testClass;
    }

    public String getTestName() {
        return testName;
    }

    public String getTestMessage() {
        return testMessage;
    }

    public void setTestClass(String testClass) {
        this.testClass = testClass;
    }

    public void setTestName(String testName) {
        this.testName = testName;
    }

    public void setTestMessage(String testMessage) {
        this.testMessage = testMessage;
    }

    public TestResult getTestResult() {
        return testResult;
    }

    public void setTestResult(TestResult testResult) {
        this.testResult = testResult;
    }

    public TestFailure() {}

    public TestFailure(String testClass, String testName, String testMessage) {
        this.testClass = testClass;
        this.testName = testName;
        this.testMessage = testMessage;
    }


    public TestFailure(TestFailure testFailure, TestResult testResult) {
        this(testFailure.testClass, testFailure.testName, testFailure.testMessage);
        this.testResult = testResult;
    }

    public String getTestMethodBody(){
        String result = "";
        String classPath = testResult.getUpload().getAssignment().getTestsSourcePath();
        String[] string1 = this.testClass.split("\\.");
        for(String s : string1){
            classPath+="/" + s;
        }
        classPath+=".java";
        System.out.println(classPath);
        try {
            result = Parser.parseMethodBody(classPath,this.testName);
            System.out.println(result);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

}
