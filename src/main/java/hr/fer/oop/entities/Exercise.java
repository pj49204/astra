package hr.fer.oop.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import hr.fer.oop.forms.ExerciseForm;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.validator.constraints.Length;
import org.springframework.beans.BeanUtils;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.*;

/**
 * Created by pavao on 10.01.17..
 */
@Entity
public class Exercise {
    @Id
    @GeneratedValue
    private Long id;

    @ManyToMany(mappedBy = "exercises", cascade = CascadeType.DETACH)
    private Set<UserGroup> userGroups;;

    @OneToMany(mappedBy = "exercise", cascade = CascadeType.REMOVE)
    @JsonManagedReference
    private List<Assignment> assignments;

    @NotNull
    @Length(min = 3)
    @Column(unique = true)
    private String name;

    @NotNull
    @DateTimeFormat(pattern="u-M-d")
    private LocalDate beginTime;

    @NotNull
    @DateTimeFormat(pattern="u-M-d")
    private LocalDate endTime;

    public Long getId() {
        return id;
    }

    public Set<UserGroup> getUserGroups() {
        return userGroups;
    }

    public List<Assignment> getAssignments() {
        return assignments;
    }

    public String getName() {
        return name;
    }

    public LocalDate getBeginTime() {
        return beginTime;
    }

    public LocalDate getEndTime() {
        return endTime;
    }

    public void setUserGroups(Set<UserGroup> userGroups) {
        this.userGroups = userGroups;
    }

    public void setAssignments(List<Assignment> assignments) {
        this.assignments = assignments;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setBeginTime(LocalDate beginTime) {
        this.beginTime = beginTime;
    }

    public void setEndTime(LocalDate endTime) {
        this.endTime = endTime;
    }

    public Calendar getCalendarBeginTime () {
        return GregorianCalendar.from(beginTime.atStartOfDay(ZoneId.systemDefault()));
    }

    public Calendar getCalendarEndTime () {
        return GregorianCalendar.from(endTime.atStartOfDay(ZoneId.systemDefault()));
    }

    public Exercise(){
        this.name = "ime labosa";
        this.beginTime = LocalDate.now().minusDays(2);
        this.endTime = LocalDate.now().plusDays(2);
        this.assignments = new ArrayList<>();
        this.userGroups = new HashSet<>();
    }

    public Exercise(Set<UserGroup> userGroups, List<Assignment> assignments, String name, LocalDate beginTime, LocalDate endTime) {
        this.userGroups = userGroups;
        this.assignments = assignments;
        this.name = name;
        this.beginTime = beginTime;
        this.endTime = endTime;
    }

    public Exercise(ExerciseForm exerciseForm) {
        BeanUtils.copyProperties(exerciseForm, this);
        this.userGroups = new HashSet<>();
        this.assignments = new ArrayList<>();
    }


    @Override
    public String toString() {
        return "Exercise{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", beginTime=" + beginTime +
                ", endTime=" + endTime +
                '}';
    }
}
