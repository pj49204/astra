package hr.fer.oop.entities;

import hr.fer.oop.forms.UserGroupForm;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.validator.constraints.Length;
import org.springframework.beans.BeanUtils;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by pavao on 10.01.17..
 */
@Entity
public class UserGroup {
    @Id
    @GeneratedValue
    private Long id;

    @ManyToMany(cascade = CascadeType.DETACH)
    @JoinTable(name = "user_user_group", joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "user_group_id", referencedColumnName = "id"))
    private Set<User> users;

    @ManyToMany(cascade = CascadeType.DETACH)
    @JoinTable(name = "exercise_user_group",
            joinColumns = @JoinColumn(name = "exercise_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "user_group_id", referencedColumnName = "id"))
    private Set<Exercise> exercises;

    @NotNull
    @Length(min = 3)
    @Column(unique = true)
    private String name;

    public Long getId() {
        return id;
    }

    public Set<User> getUsers() {
        return users;
    }

    public Set<Exercise> getExercises() {
        return exercises;
    }

    public String getName() {
        return name;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }

    public void setExercises(Set<Exercise> exercises) {
        this.exercises = exercises;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UserGroup(Set<User> users, Set<Exercise> exercises, String name) {
        this.users = users;
        this.exercises = exercises;
        this.name = name;
    }

    public UserGroup(String name) {
        this.name = name;
        this.exercises = new HashSet<>();
        this.users = new HashSet<>();
    }

    public UserGroup(UserGroupForm userGroupForm) {
        BeanUtils.copyProperties(userGroupForm, this);
        this.exercises = new HashSet<>();
        this.users = new HashSet<>();
    }

    public UserGroup() {

    }

    @Override
    public String toString() {
        return "UserGroup{" +
                "name='" + name + '\'' +
                '}';
    }
}
