package hr.fer.oop.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import hr.fer.oop.forms.AssignmentForm;
import hr.fer.oop.utils.unzip.UnzipFileResult;
import org.apache.tomcat.jni.Local;
import org.hibernate.validator.constraints.Length;
import org.springframework.beans.BeanUtils;

import javax.persistence.*;

import javax.validation.constraints.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

@Entity
//@Table(uniqueConstraints = @UniqueConstraint(columnNames = "test_result_id"))
public class Upload {

//    @OneToMany(mappedBy = "assignment", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
//    private List<TestResult> testResults;

    /**
     * Enum representing current Upload state
     */
    public static enum UploadState{
        /**
         * Guuud
         */
        GOOD,
        /**
         * beeed
         */
        BAD
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @NotNull
    private String runnablePath;
    
    @NotNull
    private String sourcePath;
    
    @ManyToOne
    @JoinColumn(name = "user_id")
    @JsonIgnore
    private User user;
    
    @ManyToOne
    @JoinColumn(name = "assignment_id")
    @JsonManagedReference
    private Assignment assignment;

    @OneToOne(mappedBy = "upload", cascade = CascadeType.REMOVE)
    @JsonIgnore
    private TestResult testResult;

    @OneToMany(mappedBy = "upload", cascade = {CascadeType.REMOVE, CascadeType.PERSIST})
    @JsonBackReference
    private List<UploadSourceEntry> uploadSourceEntries;


    private Upload.UploadState uploadState;

    @NotNull
    private LocalDateTime dateTime;
    
    public Upload() {
    }

    public Upload(String runnablePath, String sourcePath) {
        this.runnablePath = runnablePath;
        this.sourcePath = sourcePath;
        this.dateTime = LocalDateTime.now();
    }

    public Upload(Map<String, String> paths) {
        this.runnablePath = paths.get("runnableFilePath");
        this.sourcePath = paths.get("sourceFilePath");
        this.dateTime = LocalDateTime.now();
    }

    public Upload(String runnablePath, UnzipFileResult unzipFileResult) {
        this.runnablePath = runnablePath;
        this.sourcePath = unzipFileResult.getDirectoryPath();
        this.uploadSourceEntries = unzipFileResult.getFileList();
        this.dateTime = LocalDateTime.now();
    }

    public String getRunnablePath() {
        return runnablePath;
    }

    public void setRunnablePath(String runnablePath) {
        this.runnablePath = runnablePath;
    }

    public String getSourcePath() {
        return sourcePath;
    }

    public void setSourcePath(String sourcePath) {
        this.sourcePath = sourcePath;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Assignment getAssignment() {
        return assignment;
    }

    public void setAssignment(Assignment assignment) {
        this.assignment = assignment;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public Long getId() {
        return id;
    }

    public TestResult getTestResult() {
        return testResult;
    }

    public void setTestResult(TestResult testResult) {
        this.testResult = testResult;
    }

    public List<UploadSourceEntry> getUploadSourceEntries() {
        return uploadSourceEntries;
    }

    public void setUploadSourceEntries(List<UploadSourceEntry> uploadSourceEntries) {
        this.uploadSourceEntries = uploadSourceEntries;
    }

    public UploadState getUploadState() {
        return uploadState;
    }

    public void setUploadState(UploadState uploadState) {
        this.uploadState = uploadState;
    }
}


