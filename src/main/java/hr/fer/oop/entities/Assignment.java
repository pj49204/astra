package hr.fer.oop.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import hr.fer.oop.forms.AssignmentForm;
import org.hibernate.validator.constraints.Length;
import org.springframework.beans.BeanUtils;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;

@Entity
public class Assignment {

    @OneToMany(mappedBy = "assignment", cascade = CascadeType.REMOVE, fetch = FetchType.LAZY)
    @JsonBackReference
    private List<Upload> uploads;

    @Id
    @GeneratedValue(strategy =   GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Length(min = 3)
    private String name;

    @NotNull
    @Column(length = 1000)
    private String description;

    @ManyToOne
    @JoinColumn(name = "exercise_id")
    @JsonBackReference
    private Exercise exercise;

    @NotNull
    private String testsPath;

    @NotNull
    private String testsSourcePath;

    public Assignment(List<Upload> uploads, String name, String description, Exercise exercise) {
        this.uploads = uploads;
        this.name = name;
        this.description = description;
        this.exercise = exercise;
    }


    public void setExercise(Exercise exercise) {
        this.exercise = exercise;
    }

    public Exercise getExercise() {

        return exercise;
    }

    public Assignment() {
    }

    public Assignment(String name, String description, LocalDateTime endTime) {
        this.name = name;
        this.description = description;
    }

    public Assignment(AssignmentForm assignmentForm) {
        BeanUtils.copyProperties(assignmentForm, this);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTestsPath() {
        return testsPath;
    }

    public void setTestsPath(String testsPath) {
        this.testsPath = testsPath;
    }

    public List<Upload> getUploads() {
        return uploads;
    }

    public void setUploads(List<Upload> uploads) {
        this.uploads = uploads;
    }

    public String getTestsSourcePath() {
        return testsSourcePath;
    }

    public void setTestsSourcePath(String testsSourcePath) {
        this.testsSourcePath = testsSourcePath;
    }

    @Override
    public String toString() {
        return "Assignment{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                '}';
    }

    public Assignment(String name, String description, Exercise exercise) {
        this.name = name;
        this.description = description;
        this.exercise = exercise;
        this.testsPath = "";
        this.testsSourcePath = "";
    }

}


