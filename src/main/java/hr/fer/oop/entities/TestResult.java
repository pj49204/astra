package hr.fer.oop.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Calendar;
import java.util.GregorianCalendar;

@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = "upload_id"))
public class TestResult {

    @OneToOne
    @JsonIgnore
    private Upload upload;

    @OneToMany(mappedBy = "testResult", cascade = CascadeType.REMOVE)
    @JsonManagedReference
    private List<TestFailure> testFailures;

    @Id
    @GeneratedValue
    private Long id;

    @NotNull
    @OneToOne
    @JsonIgnore
    private User user;

    @NotNull
    private LocalDateTime dateTime;

    @NotNull
    private Long testsStarted;

    @NotNull
    private Long testsSuccessful;

    @NotNull
    private Long testsFailed;

    public Long getId() {
        return id;
    }

    public User getUser() {
        return user;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public Long getTestsStarted() {
        return testsStarted;
    }

    public Long getTestsSuccessful() {
        return testsSuccessful;
    }

    public Long getTestsFailed() {
        return testsFailed;
    }

    public List<TestFailure> getTestFailures() {
        return testFailures;
    }

    public Calendar getCalendarTime () {
        ZonedDateTime zdt = ZonedDateTime.of(dateTime, ZoneId.systemDefault());
        return GregorianCalendar.from(zdt);
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public void setTestsStarted(Long testsStarted) {
        this.testsStarted = testsStarted;
    }

    public void setTestsSuccessful(Long testsSuccessful) {
        this.testsSuccessful = testsSuccessful;
    }

    public void setTestsFailed(Long testsFailed) {
        this.testsFailed = testsFailed;
    }

    public void setTestFailures(List<TestFailure> testFailures) {
        this.testFailures = testFailures;
    }

    public Upload getUpload() {
        return upload;
    }

    public void setUpload(Upload upload) {
        this.upload = upload;
    }

    public TestResult () {}

    public TestResult(Upload upload, User user, LocalDateTime dateTime, Long testsStarted, Long testsSuccessful,Long testsFailed, List<TestFailure> testFailures) {
        this.upload = upload;
        this.user = user;
        this.dateTime = dateTime;
        this.testsStarted = testsStarted;
        this.testsSuccessful = testsSuccessful;
        this.testsFailed = testsFailed;
        this.testFailures = testFailures;
    }
}
