package hr.fer.oop.entities;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by ilakovac on 20.01.17..
 */
@Entity
public class UploadSourceEntry {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    private String path;

    @NotNull
    private String relativePath;

    @NotNull
    private String filename;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "upload_id")
    @JsonManagedReference
    private Upload upload;

    public UploadSourceEntry(String path, String relativePath, String filename) {
        this.path = path;
        this.relativePath = relativePath;
        this.filename = filename;
    }

    public UploadSourceEntry () {}

    public Long getId() {
        return id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getRelativePath() {
        return relativePath;
    }

    public void setRelativePath(String relativePath) {
        this.relativePath = relativePath;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public Upload getUpload() {
        return upload;
    }

    public void setUpload(Upload upload) {
        this.upload = upload;
    }

    @Override
    public String toString() {
        return "UploadSourceEntry{" +
                "id=" + id +
                ", filename='" + filename + '\'' +
                '}';
    }
}
