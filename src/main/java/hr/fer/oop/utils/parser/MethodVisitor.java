package hr.fer.oop.utils.parser;

import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.expr.AnnotationExpr;
import com.github.javaparser.ast.visitor.VoidVisitorAdapter;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

/**
 * Created by pavao on 14.01.17..
 */
public class MethodVisitor extends VoidVisitorAdapter{
    private Map<String, String> methods = new HashMap<>();
    @Override
    public void visit(MethodDeclaration methodDeclaration, Object arg){
        final String[] annotations = {""};
        methodDeclaration.getAnnotations().forEach(annotationExpr -> annotations[0]+=annotationExpr + "\n");
        methods.put(
                methodDeclaration.getNameAsString(),
                methodDeclaration.getBody().isPresent()
                        ? annotations[0] + methodDeclaration.getDeclarationAsString() + methodDeclaration.getBody().get().toString()
                        : "{\n\n}"
        );
    }

    public String getMethodBody(String methodName) {
        return methods.get(methodName);
    }
}
