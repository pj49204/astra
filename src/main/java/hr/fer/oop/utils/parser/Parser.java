package hr.fer.oop.utils.parser;

import com.github.javaparser.JavaParser;
import com.github.javaparser.ast.CompilationUnit;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by pavao on 14.01.17..
 */
public class Parser {

    public static String  parseMethodBody(String classPath, String methodName) throws IOException {
        CompilationUnit cu = null;
        try (InputStream in = new FileInputStream(classPath)) {
            cu = JavaParser.parse(in);
        } catch (Exception e) {
            e.printStackTrace();
        }
        MethodVisitor methodVisitor = new MethodVisitor();
        methodVisitor.visit(cu,null);
        return methodVisitor.getMethodBody(methodName);
    }
}
