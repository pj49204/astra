package hr.fer.oop.utils.unzip;

import hr.fer.oop.entities.UploadSourceEntry;

import java.util.List;

/**
 * Created by ilakovac on 20.01.17..
 */
public class UnzipFileResult {
    private String directoryPath;
    private List<UploadSourceEntry> fileList;


    public UnzipFileResult(String directoryPath, List<UploadSourceEntry> fileList) {
        this.directoryPath = directoryPath;
        this.fileList = fileList;
    }

    public String getDirectoryPath() {
        return directoryPath;
    }

    public void setDirectoryPath(String directoryPath) {
        this.directoryPath = directoryPath;
    }

    public List<UploadSourceEntry> getFileList() {
        return fileList;
    }

    public void setFileList(List<UploadSourceEntry> fileList) {
        this.fileList = fileList;
    }
}
