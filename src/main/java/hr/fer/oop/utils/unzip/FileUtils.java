package hr.fer.oop.utils.unzip;

import hr.fer.oop.entities.UploadSourceEntry;
import org.apache.commons.io.FilenameUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.security.MessageDigest;
import java.util.*;

public class FileUtils {

    private Set<String> allowedFileTypes;

    private String directory;

    public FileUtils (String directory, String ...allowedFileTypes) {
        this.allowedFileTypes = new HashSet<>();
        Collections.addAll(this.allowedFileTypes, allowedFileTypes);
        this.directory = directory;
    }

    public String uploadFile(MultipartFile file, String name) throws Exception {
        if (file.isEmpty()) {
            throw new Exception("Invalid file provided");
        }
        if (!allowedFileTypes.contains(file.getContentType())) {
            throw new Exception("Invalid file type provided (" + file.getContentType() + ", " + file.getOriginalFilename() + ")");
        }
        String filename = file.getOriginalFilename();
        String extension = FilenameUtils.getExtension(filename);

        if (!Files.exists(Paths.get(directory))) {
            Files.createDirectory(Paths.get(directory));
        }

        String hash = MD5(System.currentTimeMillis() + "" + directory);

        String filepath = Paths.get(directory,  name + "-" + hash + "." + extension).toString();

        try (BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(new File(filepath)))) {
            stream.write(file.getBytes());
        }

        return filepath;
    }

    private String MD5(String input) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] array = md.digest(input.getBytes());
            StringBuilder sb = new StringBuilder();
            for (byte b : array) {
                sb.append(Integer.toHexString((b & 0xFF) | 0x100).substring(1,3));
            }
            return sb.toString();
        } catch (java.security.NoSuchAlgorithmException e) {
        }
        return null;
    }

    private FileSystem createZipFileSystem (String path) throws IOException {
        URI uri = URI.create("jar:" + Paths.get(path).toUri().toString());
        Map<String, String> env = new HashMap<>();
        env.put("create", "false");
        return FileSystems.newFileSystem(uri, env);
    }

    public UnzipFileResult unzipFile(MultipartFile file, String name) throws Exception {
        String zipFileName = uploadFile(file, name);
        String baseName = FilenameUtils.getBaseName(zipFileName);
        Path directoryPath = Paths.get(directory + "/" + baseName);
        if (!Files.exists(directoryPath)) {
            Files.createDirectory(directoryPath);
        }
        UnzipFileVisitor unzipFileVisitor = new UnzipFileVisitor(directoryPath);
        try (FileSystem zipFileSystem = createZipFileSystem(zipFileName)) {
            Path root = zipFileSystem.getPath("/");
            Files.walkFileTree(root, unzipFileVisitor);
        } catch (IOException e) {
            throw new Exception("Error while unziping the file");
        } finally {
            Files.deleteIfExists(Paths.get(zipFileName));
        }
        System.out.println(unzipFileVisitor.getFileList());
        return new UnzipFileResult(directoryPath.toString(), unzipFileVisitor.getFileList());
    }

    private static class UnzipFileVisitor extends SimpleFileVisitor<Path> {

        private Path directoryPath;
        private List<UploadSourceEntry> fileList = new ArrayList<>();

        UnzipFileVisitor (Path directoryPath) {
            this.directoryPath = directoryPath;
        }

        @Override
        public FileVisitResult visitFile(Path file,
                BasicFileAttributes attrs) throws IOException {
            Path destFile = Paths.get(directoryPath.toString(), file.toString());
            fileList.add(
                    new UploadSourceEntry(destFile.toString(),file.toString(), file.getFileName().toString())
            );
            System.out.printf("Extracting file %s to %s\n", file, destFile);
            Files.copy(file, destFile, StandardCopyOption.REPLACE_EXISTING);
            return FileVisitResult.CONTINUE;
        }

        @Override
        public FileVisitResult preVisitDirectory(Path dir,
                BasicFileAttributes attrs) throws IOException {
            Path directory = Paths.get(directoryPath.toString(), dir.toString());
            if(!Files.exists(directory)){
                System.out.printf("Creating directory %s\n", directory);
                Files.createDirectory(directory);
            }
            return FileVisitResult.CONTINUE;
        }

        public List<UploadSourceEntry> getFileList() {
            return fileList;
        }
    }
}
