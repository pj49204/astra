package hr.fer.oop;

import hr.fer.oop.Calculator;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * JUnit 5 test
 */

public class CalulatorAdditionTest {
    @Test
    public void testAddition1(){
        int result = Calculator.add(5,3);
        assertEquals(8,result);
    }
    @Test
    public void testAddition2(){
        int result = Calculator.add(1,0);
        assertEquals(1,result);
    }

    @Test
    public void testAddition3(){
        int result = Calculator.add(2,3);
        assertEquals(5,result);
    }


    @Test
    @DisplayName("Test -1 + 0")
    public void testAddition4(){
        int result = Calculator.add(-1,0);
        assertEquals(-1,result);
        assertTrue(false);
    }
}