package hr.fer.oop;

import hr.fer.oop.Calculator;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

/**
 * JUnit 4 test
 */
public class CalculatorDivisionTest {
    @Test(expected = ArithmeticException.class)
    public void testDivision1() {
        double result = Calculator.divise(1,0);
    }

    @Test
    public void testDivision2() {
        double result = Calculator.divise(1,1);
        assertEquals(1.0,result,0.0001);
    }

    @Test
    public void testDivision3() {
        double result = Calculator.divise(6,2);
        assertEquals(3.0,result,0.0001);
    }

    @Test
    public void testDivision4() {
        double result = Calculator.divise(7,2);
        assertEquals(3.5,result,0.0001);
    }
}