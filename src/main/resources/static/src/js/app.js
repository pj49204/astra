var Utilities = function () {
    var parseResponse = function (response) {
        try {
            if (typeof response.responseText !== "undefined")
                response = response.responseText;
            else if (typeof response === "object")
                return response;
            return JSON.parse(response);
        } catch (e) {
            return null;
        }
    };


    var escapeHtml = function (unsafe) {
        return unsafe
            .replace(/&/g, "&amp;")
            .replace(/</g, "&lt;")
            .replace(/>/g, "&gt;")
            .replace(/"/g, "&quot;")
            .replace(/'/g, "&#039;");
    };

    return {
        parseResponse: parseResponse,
        escapeHtml: escapeHtml
    };
}();
var Messages = function () {
    var el;
    var init = function () {
        el = $('#messages');
    };
    // type: success, danger, warning, info
    var add = function (message, type) {
        el.append(
            $('<div class="alert alert-'+ type + '" role="alert">' +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                '</button>' +
                message +
            '</div>')
        );
        $(".alert").alert();
    };
    return {
        init: init,
        add: add
    };
}();
var App = function () {
    var modules = {};
    var __i18n__ = {
        unknownErrorWhileRunningTests: 'Došlo je do greške prilikom pokretanja testova.',
        hideFailingMethod: "Sakrij metodu",
        showFailingMethod: "Pokaži metodu",
        successfulUpload: 'Datoteka uspješno prenesena.',
        successfulTestsStart: 'Testovi uspješno pokrenuti',
        successfulTesting: 'Testovi uspješno završeni.',
        unknownErrorWhileUploading: 'Došlo je do greške prilikom prijenosa datoteke.',
        areYouSure: 'Jeste li sigurni?'
    };
    modules.testResultsModal = function (el) {
        $(el).on('click', function (e) {
            $.get('/test_results/' + $(el).data('id'), function (response) {
                var $modal = $('#testResultsModal').modal();
                var $testFailures = $('#testResultsModal').find('.test-failures');
                $testFailures.html('');
                var failuresString = "";
                response.testFailures.forEach(function (failure) {
                    failuresString += '<tr>' +
                    '<td>' + Utilities.escapeHtml(failure.testClass) + '</td>' +
                    '<td>' + Utilities.escapeHtml(failure.testMessage) + '</td>' +
                    '<td>' +
                        '<a href="#" class="btn btn-secondary show-method-body" data-id="' + failure.testFailureId + '">' +
                            __i18n__.showFailingMethod +
                        '</a>' +
                    '</td>' +
                    '</tr>';
                    failuresString += '<tr class="hidden" id="failureMethodBody-'+ failure.testFailureId +'">' +
                        '<td colspan="3">' +
                            '<pre><code class="java method-body">' +
                                failure.testMethodBody +
                            '</code></pre>' +
                        '</td>' +
                        '</tr>';
                });
                $testFailures.html(failuresString);
                $testFailures.on('click', '.show-method-body', function (e) {
                    var id = $(this).data('id');
                    var $el = $('#failureMethodBody-'+ id);
                    if (!$(this).data('shown')) {
                        var $body = $el.find('.method-body');
                        hljs.highlightBlock($body[0]);
                        $(this).data('shown', true);
                        $(this).html(__i18n__.hideFailingMethod);
                    } else {
                        $(this).removeData('shown');
                        $(this).html(__i18n__.showFailingMethod);
                    }
                    $el.toggleClass("hidden");
                    e.preventDefault();
                });
                $modal.modal('show');
            });
            e.preventDefault();
        })
    };
    modules.calendarField = function (el) {
         new Pikaday({
             field: el,
             format: 'YYYY-MM-DD',
         });
    };

    modules.sourceCodeViewer = function (el) {
        console.log("SOURCE CODE VIEWER");
        var $list = $(el).find(".list-group");
        var $sourceCode = $(el).find(".source-code");
        var $items = $list.find('a.list-group-item');
        console.log($list, $sourceCode, $items);
        $items.on('click', function (e) {
            $items.each(function (item) {
                $(item).removeClass('active');
            });
            var id = $(this).data('id');
            $(this).addClass('active');
            $.get('/upload_source_entry/' + id).then(function (response) {
                $sourceCode.html(response.source);
                hljs.highlightBlock($sourceCode[0]);
            });
            e.preventDefault();
        });
    };

    modules.confirmSubmit = function (el) {
        $(el).on('submit', function (e) {
            return confirm(__i18n__.areYouSure);
        })
    };
    
    modules.ajaxForm = function (el) {
        $(el).on('submit', function (e) {
            // Send via ajax
            var url = $(el).attr('action');
            console.log(url);
            var $btn = $(this).find('.upload-button');
            $btn.attr('disabled', true);
            $.ajax({
                xhr: function () {
                    var xhr = new window.XMLHttpRequest();
                    xhr.upload.addEventListener('progress', function (evt) {
                        if (evt.lengthComputable) {
                            var percentComplete = evt.loaded / evt.total;
                            if (percentComplete === 1) {
                                // Complete
                            }
                        }
                    }, false);
                    xhr.addEventListener('progress', function (evt) {
                        if (evt.lengthComputable) {
                            var percentComplete = evt.loaded / evt.total;
                        }
                    }, false);
                    return xhr;
                },
                url: url,
                method: 'POST',
                data: new FormData(el),
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false,
                cache: false
            }).then(function (response) {
                response = Utilities.parseResponse(response);
                if (response && response.success) {
                    Messages.add(__i18n__.successfulUpload, 'success');
                    Messages.add(__i18n__.successfulTestsStart, 'success');
                    var upload = response.upload;
                    $.post('/test/' + upload.id).then(function (response) {
                        Messages.add(__i18n__.successfulTesting, 'success');
                        setTimeout(function () {
                            window.location.href = '/assignment/' + upload.assignment.id;
                        }, 1000);
                    }, function (error) {
                        Messages.add(__i18n__.unknownErrorWhileRunningTests, 'error')
                    })
                } else {
                    Messages.add(__i18n__.unknownErrorWhileUploading, 'danger');
                }
                el.reset()
            }, function (error) {
                error = Utilities.parseResponse(error);
                Messages.add(error && error.error ? error.error : __i18n__.unknownErrorWhileUploading, 'danger');
                el.reset();
            }).always(function () {
                $btn.attr('disabled', false);
            });
            e.preventDefault();
        });
    };
    modules.init = function () {

        var invokeElements = $('[data-app]');
        if (invokeElements.length === 0) return;
        invokeElements.each(function (index, element) {
            var apps = $(element).data('app');
            // Invoke element surely has a data-app property
            if (apps === '') return;
            apps = apps.split(' ');
            apps.forEach(function (app) {
                if (typeof modules[app] !== 'undefined') modules[app](element);
            });
        });
    };
    return modules;
}();

// jQUery ready
$(function () {
    Messages.init();
    App.init();
});