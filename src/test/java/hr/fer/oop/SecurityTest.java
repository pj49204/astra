package hr.fer.oop;

import hr.fer.oop.configurations.WebSecurityConfiguration;
import hr.fer.oop.controllers.HomeController;
import hr.fer.oop.controllers.LoginController;
import hr.fer.oop.controllers.TestController;
import hr.fer.oop.controllers.UploadController;
import hr.fer.oop.entities.User;
import hr.fer.oop.handlers.CustomAccessDeniedHandler;
import hr.fer.oop.repositories.UserRepository;
import hr.fer.oop.services.LoginService;
import hr.fer.oop.services.jpa.LoginServiceImplementation;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import java.util.logging.Filter;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;


/**
 * Created by pavao on 06.12.16..
 */
@RunWith(value = SpringRunner.class)
@WebMvcTest(controllers = {TestController.class, LoginController.class, HomeController.class})
@Import(value = {SecurityTest.TestConfiguration.class})
public class SecurityTest {

    private MockMvc mvc;


    @Configuration
    static class TestConfiguration {
        @Bean
        public PasswordEncoder passwordEncoder() {
            PasswordEncoder encoder = new BCryptPasswordEncoder();
            return encoder;
        }
    }


    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private FilterChainProxy filterChainProxy;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        mvc = MockMvcBuilders
                .webAppContextSetup(webApplicationContext)
                .dispatchOptions(true)
                .apply(springSecurity(filterChainProxy))
                .build();
    }
//
//
//    @Test
//    @WithAnonymousUser
//    public void anonymousAccessOnHome() throws Exception {
//        /*MockMvc mvc = MockMvcBuilders
//                .standaloneSetup(new UploadController())
//                .setViewResolvers(new InternalResourceViewResolver("/WEB-INF/jsp/view/", ".jsp"))
//                .build();*/
//        mvc.perform(get("/").header("host", "localhost:1337"))
//                .andDo(print())
//                .andExpect(status().isOk());
//    }
//
    @Test
    @WithAnonymousUser
    public void unauthorizedAccessOnTest() throws Exception {
        /*MockMvc mvc = MockMvcBuilders
                .standaloneSetup(new TestController())
                .setViewResolvers(new InternalResourceViewResolver("/WEB-INF/jsp/view/", ".jsp"))
                .apply(springSecurity(springSecurityFilterChain))
                .build();*/
        mvc.perform(get("/test"))
                .andDo(print())
                .andExpect(status().isUnauthorized());
    }
//
//    @Test
//    @WithMockUser
//    public void authorizedAccessOnLogin() throws Exception {
//        /*MockMvc mvc = MockMvcBuilders
//                .standaloneSetup(new LoginController())
//                .setViewResolvers(new InternalResourceViewResolver("/WEB-INF/jsp/view/", ".jsp"))
//                .build();*/
//        mvc.perform(get("/login").with(user("user").password("password").roles("USER")))
//                .andDo(print())
//                .andExpect(status().isForbidden());
//    }
//
//    @Test
//    @WithAnonymousUser
//    public void unauthorizedAccessOnRegistration() throws  Exception{
//        mvc.perform(get("/registration"))
//                .andDo(print())
//                .andExpect(status().isOk());
//    }
//
//    @Test
//    @WithMockUser
//    public void authorizedAccessOnRegistration() throws Exception{
//        mvc.perform(get("/registration"))
//                .andDo(print())
//                .andExpect(status().isForbidden());
//    }
}