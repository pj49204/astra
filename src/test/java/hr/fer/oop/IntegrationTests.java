package hr.fer.oop;

import hr.fer.oop.configurations.WebSecurityConfiguration;
import hr.fer.oop.controllers.HomeController;
import hr.fer.oop.entities.User;
import hr.fer.oop.repositories.UserRepository;
import hr.fer.oop.security.CustomUserDetails;
import hr.fer.oop.services.LoginService;
import hr.fer.oop.services.jpa.LoginServiceImplementation;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MockMvcBuilder;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import static org.hamcrest.Matchers.is;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.isNull;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders.formLogin;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;

/**
 * Created by pavao on 16.12.16..
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc(secure = false)
@ActiveProfiles(profiles = "dev")
public class IntegrationTests {

    @Autowired
    private MockMvc mvc;

    @Test
    public void homeControllerTest() throws Exception {
        mvc.perform(get("/"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("home"));
    }

    private MockHttpSession session;

    @Before
    public void setup() throws Exception{
        session = (MockHttpSession) mvc.perform(formLogin().user("admin").password("admin")).andReturn()
                .getRequest().getSession();
    }

    @Test
    public void registrationControllerSuccessTest() throws Exception {
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.set("firstName", "Ime");
        params.set("lastName", "Prezime");
        params.set("JMBAG", "0123456789");
        params.set("username", "Korisnik");
        params.set("password", "Lozinka1");
        mvc.perform(post("/registration").params(params))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(model().attribute("errorMessage", is("CONGRATULATIONS!!\n")));
    }

    @Test
    public void registrationControllerPasswordIsTooShortTest() throws Exception{
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.set("firstName", "Ime");
        params.set("lastName", "Prezime");
        params.set("JMBAG", "0123456789");
        params.set("username", "Korisnik");
        params.set("password", "Lozinka");
        mvc.perform(post("/registration").params(params))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(model().attribute("errorMessage", is("Field PASSWORD is too short\n")));
    }

    @Test
    public void registrationControllerStartTest() throws Exception{
        mvc.perform(get("/registration"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(model().attribute("errorMessage", isNull()));
    }


    @Test
    public void assignmentsControllerShowTest() throws Exception{
        mvc.perform(get("/assignment/1").secure(false).session(session))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void assignmentControllerCreateTest() throws Exception{

    }



}
