package hr.fer.oop;

import hr.fer.oop.entities.User;
import hr.fer.oop.repositories.UserRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.boot.test.autoconfigure.orm.jpa.*;

import static org.assertj.core.api.Assertions.*;

/**
 * Created by pavao on 05.12.16..
 */

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@Import(UserRepositoryTest.TestConfiguration.class)
public class UserRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private UserRepository userRepository;


    @Configuration
    public static class TestConfiguration{
        @Bean
        public static PasswordEncoder passwordEncoder(){
            PasswordEncoder encoder = new BCryptPasswordEncoder();
            return encoder;
        }
    }

    @Test
    public void userRepoTest1(){
        entityManager.persist(new User("Korisnik", TestConfiguration.passwordEncoder().encode("Lozinka") , "USER"));
        User user = userRepository.findByUsername("Korisnik");
        assertThat(user).isNotNull();
        assertThat(user.getJMBAG()).isEqualTo("0000000000");
        assertThat(user.getFirstName()).isEqualTo("FirstName");
        assertThat(user.getRole()).isEqualTo("USER");
        assertThat(TestConfiguration.passwordEncoder().matches("Lozinka",user.getPassword())).isTrue();

    }

    @Test
    public void userRepoTest2(){
        entityManager.persist(new User("Administrator", TestConfiguration.passwordEncoder().encode("Lozinka2") , "ADMIN"));
        User user = userRepository.findByUsername("Administrator");
        assertThat(user).isNotNull();
        assertThat(user.getJMBAG()).isEqualTo("0000000000");
        assertThat(user.getFirstName()).isEqualTo("FirstName");
        assertThat(user.getRole()).isEqualTo("ADMIN");
        assertThat(TestConfiguration.passwordEncoder().matches("Lozinka2",user.getPassword())).isTrue();

    }

    @Test
    public void userRepoTest3(){
        entityManager.persist(new User("Korisnik", TestConfiguration.passwordEncoder().encode("Lozinka") , "USER", "Ime" , "Prezime", "0123456789"));
        User user = userRepository.findByUsername("Korisnik");
        assertThat(user).isNotNull();
        assertThat(user.getJMBAG()).isEqualTo("0123456789");
        assertThat(user.getFirstName()).isEqualTo("Ime");
        assertThat(user.getRole()).isEqualTo("USER");
        assertThat(TestConfiguration.passwordEncoder().matches("Lozinka",user.getPassword())).isTrue();

    }
}
