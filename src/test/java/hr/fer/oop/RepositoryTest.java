package hr.fer.oop;

import hr.fer.oop.entities.*;
import hr.fer.oop.forms.AssignmentForm;
import hr.fer.oop.forms.ExerciseForm;
import hr.fer.oop.repositories.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.Column;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by pavao on 19.01.17..
 */
@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@Import(UserRepositoryTest.TestConfiguration.class)
public class RepositoryTest {

    @Autowired
    private AssignmentRepository assignmentRepository;

    @Autowired
    private ExerciseRepository exerciseRepository;

    @Autowired
    private TestFailureRepository testFailureRepository;

    @Autowired
    private TestResultRepository testResultRepository;

    @Autowired
    private UploadRepository uploadRepository;

    @Autowired
    private UserGroupRepository userGroupRepository;

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private UserRepository userRepository;


    @Configuration
    public static class TestConfiguration{
        @Bean
        public static PasswordEncoder passwordEncoder(){
            PasswordEncoder encoder = new BCryptPasswordEncoder();
            return encoder;
        }
    }

    @Test
    public void userRepoTest1(){
        entityManager.persist(new User("Korisnik", UserRepositoryTest.TestConfiguration.passwordEncoder().encode("Lozinka") , "USER"));
        User user = userRepository.findByUsername("Korisnik");
        assertThat(user).isNotNull();
        assertThat(user.getJMBAG()).isEqualTo("0000000000");
        assertThat(user.getFirstName()).isEqualTo("FirstName");
        assertThat(user.getRole()).isEqualTo("USER");
        assertThat(UserRepositoryTest.TestConfiguration.passwordEncoder().matches("Lozinka",user.getPassword())).isTrue();

    }

    @Test
    public void userRepoTest2(){
        entityManager.persist(new User("Administrator", UserRepositoryTest.TestConfiguration.passwordEncoder().encode("Lozinka2") , "ADMIN"));
        User user = userRepository.findByUsername("Administrator");
        assertThat(user).isNotNull();
        assertThat(user.getJMBAG()).isEqualTo("0000000000");
        assertThat(user.getFirstName()).isEqualTo("FirstName");
        assertThat(user.getRole()).isEqualTo("ADMIN");
        assertThat(UserRepositoryTest.TestConfiguration.passwordEncoder().matches("Lozinka2",user.getPassword())).isTrue();

    }

    @Test
    public void userRepoTest3(){
        entityManager.persist(new User("Korisnik", UserRepositoryTest.TestConfiguration.passwordEncoder().encode("Lozinka") , "USER", "Ime" , "Prezime", "0123456789"));
        User user = userRepository.findByUsername("Korisnik");
        assertThat(user).isNotNull();
        assertThat(user.getJMBAG()).isEqualTo("0123456789");
        assertThat(user.getFirstName()).isEqualTo("Ime");
        assertThat(user.getRole()).isEqualTo("USER");
        assertThat(UserRepositoryTest.TestConfiguration.passwordEncoder().matches("Lozinka",user.getPassword())).isTrue();

    }

    @Test
    public void assignmentRepoTest1(){
        AssignmentForm assignmentForm = new AssignmentForm();
        assignmentForm.setName("ime");
        assignmentForm.setDescription("opis");
        assignmentForm.setTestsPath("testPath");
        assignmentForm.setTestsSourcePath("srcPath");
        Assignment assignment = assignmentRepository.save(new Assignment(assignmentForm));
        Assignment assignmentDB = assignmentRepository.findOne(assignment.getId());
        assertThat(assignmentDB).isNotNull();
        assertThat(assignmentDB.getName()).isEqualTo(assignmentForm.getName()).isEqualTo("ime");
        assertThat(assignmentDB.getDescription()).isEqualTo(assignmentForm.getDescription()).isEqualTo("opis");
        assertThat(assignmentDB.getTestsPath()).isEqualTo(assignmentForm.getTestsPath()).isEqualTo("testPath");
        assertThat(assignmentDB.getTestsSourcePath()).isEqualTo(assignmentForm.getTestsSourcePath()).isEqualTo("srcPath");
    }

    @Test
    public void exerciseRepoTest1(){
        ExerciseForm exerciseForm = new ExerciseForm();
        exerciseForm.setName("ime");
        exerciseForm.setBeginTime(LocalDate.now());
        exerciseForm.setEndTime(LocalDate.now());
        Exercise exercise = exerciseRepository.save(new Exercise(exerciseForm));
        Exercise exerciseDB = exerciseRepository.findOne(exercise.getId());
        assertThat(exerciseDB).isNotNull();
        assertThat(exerciseDB.getName()).isEqualTo(exercise.getName()).isEqualTo("ime");
        assertThat(exerciseDB.getBeginTime()).isEqualTo(exercise.getBeginTime());
        assertThat(exerciseDB.getEndTime()).isEqualTo(exerciseDB.getEndTime());

    }

    @Test
    public void testFailureRepoTest1(){
        TestFailure testFailure = testFailureRepository.save(new TestFailure("class","method","message"));
        TestFailure testFailureDB = testFailureRepository.findOne(testFailure.getTestFailureId());
        assertThat(testFailureDB).isNotNull();
        assertThat(testFailureDB.getTestClass()).isEqualTo(testFailure.getTestClass());
        assertThat(testFailureDB.getTestName()).isEqualTo(testFailure.getTestName());
        assertThat(testFailureDB.getTestMessage()).isEqualTo(testFailure.getTestMessage());
    }

    @Test
    public void testResultRepoTest1(){
        User user = userRepository.save(new User());
        TestResult testResult = testResultRepository.save(new TestResult(new Upload(), user, LocalDateTime.now(), 8L, 7L, 1L, new ArrayList<TestFailure>()));
        TestResult testResultDB = testResultRepository.findOne(testResult.getId());
        assertThat(testResultDB).isNotNull();
        assertThat(testResultDB.getDateTime()).isEqualTo(testResult.getDateTime());
        assertThat(testResultDB.getTestsStarted()).isEqualTo(testResult.getTestsStarted());
        assertThat(testResultDB.getTestsSuccessful()).isEqualTo(testResult.getTestsSuccessful());
        assertThat(testResultDB.getTestsFailed()).isEqualTo(testResult.getTestsFailed());

    }

    @Test
    public void uploadRepoTest1(){
        Upload upload = uploadRepository.save(new Upload("rn","src"));
        Upload uploadDB = uploadRepository.findOne(upload.getId());
        assertThat(uploadDB).isNotNull();
        assertThat(uploadDB.getRunnablePath()).isEqualTo(upload.getRunnablePath());
        assertThat(uploadDB.getSourcePath()).isEqualTo(upload.getSourcePath());
    }

    @Test
    public void userGroupRepoTest1(){
        UserGroup userGroup = new UserGroup("ime");
        entityManager.persist(userGroup);
        UserGroup userGroupDB = userGroupRepository.findByName("ime");
        assertThat(userGroupDB).isNotNull();
        assertThat(userGroupDB.getName()).isEqualTo(userGroup.getName());

    }
}
