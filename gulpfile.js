// Load plugins
var gulp = require('gulp'),
    minifycss = require('gulp-minify-css'),
    uglify = require('gulp-uglify'),
    concat = require('gulp-concat'),
    size = require('gulp-size'),
    merge = require('merge-stream'),
    sass = require('gulp-sass');

var dest = {
    css: 'src/main/resources/static/css',
    js: 'src/main/resources/static/js'
};

// Styles
gulp.task('styles', function() {
    var css = gulp.src([
            'node_modules/pikaday/css/pikaday.css',
            'src/main/resources/static/src/css/monokai-sublime.css'
        ]);
    var scss = gulp.src('src/main/resources/static/src/css/main.scss')
        .pipe(sass().on('error', sass.logError));
    return merge(css, scss)
        .pipe(minifycss())
        .pipe(concat('app.css'))
        .pipe(size("CSS files"))
        .pipe(gulp.dest(dest.css));
});

// Scripts
gulp.task('scripts', function() {
    return gulp.src([
            'node_modules/jquery/dist/jquery.js',
            'node_modules/tether/dist/js/tether.js',
            // Bootstrap modules
            'node_modules/bootstrap/dist/js/bootstrap.js',
            //
            'node_modules/moment/min/moment.min.js',
            'node_modules/pikaday/pikaday.js',
            'src/main/resources/static/src/js/highlight.pack.js',
            'src/main/resources/static/src/js/app.js'
        ])
        .pipe(concat('app.js'))
        .pipe(uglify())
        .pipe(size("JS files"))
        .pipe(gulp.dest(dest.js));
});

// Default task
gulp.task('default', ['styles', 'scripts']);

// Watch
gulp.task('watch', function() {
    // Watch .scss files
    gulp.watch('src/main/resources/static/src/css/*.*', function(event) {
        console.log('File ' + event.path + ' was ' + event.type + ', running tasks...');
        gulp.run('styles');
    });

    // Watch .js files
    gulp.watch('src/main/resources/static/src/js/*.js', function(event) {
        console.log('File ' + event.path + ' was ' + event.type + ', running tasks...');
        gulp.run('scripts');
    });
});