dbname=astradb
username=astra

sudo apt-get update -y
sudo apt-get install postgresql postgresql-contrib -y
sudo -i -u postgres createuser -P -d -l $username
sudo -i -u postgres createdb -U $username $dbname -W -h localhost
